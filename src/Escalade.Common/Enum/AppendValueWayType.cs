using System.ComponentModel;

namespace Escalade.Common
{
    /// <summary>
    /// 追加值的方式类型
    /// </summary>
    public enum AppendValueWayType
    {
        /// <summary>
        /// 之后
        /// </summary>
        [Description("之后")]
        After = 0,

        /// <summary>
        /// 之前
        /// </summary>
        [Description("之前")]
        Before = 1
    }
}
