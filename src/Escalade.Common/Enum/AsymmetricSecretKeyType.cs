using System.ComponentModel;

namespace Escalade.Common
{
    /// <summary>
    /// 非对称密钥类型
    /// </summary>
    public enum AsymmetricSecretKeyType
    {
        /// <summary>
        /// 公钥
        /// </summary>
        [Description("公钥")]
        PublicKey = 0,

        /// <summary>
        /// 私钥
        /// </summary>
        [Description("私钥")]
        PrivateKey = 1
    }
}
