using System.ComponentModel;

namespace Escalade.Common
{
    /// <summary>
    /// Boolean类型
    /// </summary>
    public enum BooleanType
    {
        /// <summary>
        /// False
        /// </summary>
        [Description("False")]
        False = 0,

        /// <summary>
        /// True
        /// </summary>
        [Description("True")]
        True = 1
    }
}
