using System.ComponentModel;

namespace Escalade.Common
{
    /// <summary>
    /// 常用哈希算法类型
    /// </summary>
    public enum HashAlgorithmType
    {
        /// <summary>
        /// MD5
        /// </summary>
        [Description("MD5")]
        MD5 = 1,

        /// <summary>
        /// SHA1
        /// </summary>
        [Description("SHA1")]
        SHA1 = 2,

        /// <summary>
        /// SHA256
        /// </summary>
        [Description("SHA256")]
        SHA256 = 3,

        /// <summary>
        /// SHA384
        /// </summary>
        [Description("SHA384")]
        SHA384 = 4,

        /// <summary>
        /// SHA512
        /// </summary>
        [Description("SHA512")]
        SHA512 = 5
    }
}
