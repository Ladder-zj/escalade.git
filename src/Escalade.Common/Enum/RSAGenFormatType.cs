using System.ComponentModel;

namespace Escalade.Common
{
    /// <summary>
    /// RSA生成格式类型
    /// </summary>
    public enum RSAGenFormatType
    {
        /// <summary>
        /// PKCS1格式
        /// </summary>
        [Description("PKCS1")]
        PKCS1 = 1,

        /// <summary>
        /// PKCS8格式
        /// </summary>
        [Description("PKCS8")]
        PKCS8 = 2
    }
}
