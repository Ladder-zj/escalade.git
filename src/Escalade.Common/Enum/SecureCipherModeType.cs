using System.ComponentModel;

namespace Escalade.Common
{
    /// <summary>
    /// 加解密模式类型
    /// </summary>
    public enum SecureCipherModeType
    {
        /// <summary>
        /// CBC模式
        /// </summary>
        [Description("CBC")]
        CBC = 1,

        /// <summary>
        /// ECB模式
        /// </summary>
        [Description("ECB")]
        ECB = 2
    }
}
