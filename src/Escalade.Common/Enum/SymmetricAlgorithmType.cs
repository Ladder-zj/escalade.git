using System.ComponentModel;

namespace Escalade.Common
{
    /// <summary>
    /// 对称加密算法类型
    /// </summary>
    public enum SymmetricAlgorithmType
    {
        /// <summary>
        /// DES
        /// </summary>
        [Description("DES")]
        DES = 1,

        /// <summary>
        /// AES
        /// </summary>
        [Description("AES")]
        AES = 2
    }
}
