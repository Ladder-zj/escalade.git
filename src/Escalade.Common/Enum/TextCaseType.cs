using System.ComponentModel;

namespace Escalade.Common
{
    /// <summary>
    /// 文本大小写类型
    /// </summary>
    public enum TextCaseType
    {
        /// <summary>
        /// 默认
        /// </summary>
        [Description("默认")]
        Default = 0,

        /// <summary>
        /// 大写
        /// </summary>
        [Description("大写")]
        Upper = 1,

        /// <summary>
        /// 小写
        /// </summary>
        [Description("小写")]
        Lower = 2
    }
}
