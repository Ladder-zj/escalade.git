using System;
using System.Diagnostics.CodeAnalysis;

namespace Escalade.Common
{
    /// <summary>
    /// Boolean相关扩展
    /// </summary>
    public static class BooleanExtensions
    {
        /// <summary>
        /// 转换为Int值
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static int ConvertToInt(this bool value)
        {
            return value ? 1 : 0;
        }

        /// <summary>
        /// 切换值
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static bool Toggle(this bool value)
        {
            return !value;
        }

        /// <summary>
        /// 转换成指定字符串
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="trueValue">True值字符串</param>
        /// <param name="falseValue">False值字符串</param>
        /// <param name="textCaseType">文本大小写类型</param>
        /// <returns></returns>
        public static string ConvertToString(this bool value, [NotNull] string trueValue = "True", [NotNull] string falseValue = "False", TextCaseType textCaseType = TextCaseType.Default)
        {
            trueValue = trueValue.NotNull();
            falseValue = falseValue.NotNull();
            if (textCaseType == TextCaseType.Upper)
            {
                return value ? trueValue.ToUpper() : falseValue.ToUpper();
            }
            else if (textCaseType == TextCaseType.Lower)
            {
                return value ? trueValue.ToLower() : falseValue.ToLower();
            }

            return value ? trueValue : falseValue;
        }

        /// <summary>
        /// 转换成指定枚举值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="trueValue">True枚举值</param>
        /// <param name="falseValue">False枚举值</param>
        /// <returns></returns>
        public static TOut ConvertToEnum<TOut>(this bool value, TOut trueValue, TOut falseValue) where TOut : Enum
        {
            return value ? trueValue : falseValue;
        }

        /// <summary>
        /// 对两个bool值进行逻辑与操作
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="compareValue">比较值</param>
        /// <returns></returns>
        public static bool And(this bool value, bool compareValue)
        {
            return value && compareValue;
        }

        /// <summary>
        /// 对两个bool值进行逻辑或操作
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="compareValue">比较值</param>
        /// <returns></returns>
        public static bool Or(this bool value, bool compareValue)
        {
            return value || compareValue;
        }
    }
}
