using System.Text;
using System.IO;
using System.Xml.Serialization;
using System;

namespace Escalade.Common
{
    /// <summary>
    /// byte[]相关扩展
    /// </summary>
    public static class ByteArrayExtensions
    {
        /// <summary>
        /// 转换成字符串，UTF8编码格式
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static string ToUTF8String(this byte[] value)
        {
            return Encoding.UTF8.GetString(value);
        }

        /// <summary>
        /// 转换成字符串，UTF8编码格式
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="index">起始索引</param>
        /// <param name="count">解码字节数</param>
        /// <returns></returns>
        public static string ToUTF8String(this byte[] value, int index, int count)
        {
            return Encoding.UTF8.GetString(value, index, count);
        }

        /// <summary>
        /// 转换成Base64字符串
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static string ToBase64String(this byte[] value)
        {
            return Convert.ToBase64String(value);
        }

        /// <summary>
        /// 转换成Stream
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static Stream ToStream(this byte[] value)
        {
            return ToMemoryStream(value);
        }

        /// <summary>
        /// 转换成Stream
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="index">起始索引</param>
        /// <param name="count">流的长度(以字节为单位)</param>
        /// <returns></returns>
        public static Stream ToStream(this byte[] value, int index, int count)
        {
            return ToMemoryStream(value, index, count);
        }

        /// <summary>
        /// 转换成MemoryStream
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static MemoryStream ToMemoryStream(this byte[] value)
        {
            return new MemoryStream(value);
        }

        /// <summary>
        /// 转换成MemoryStream
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="index">起始索引</param>
        /// <param name="count">流的长度(以字节为单位)</param>
        /// <returns></returns>
        public static MemoryStream ToMemoryStream(this byte[] value, int index, int count)
        {
            return new MemoryStream(value, index, count);
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static TOut DeserializeObject<TOut>(this byte[] value)
        {
            using var stream = new MemoryStream(value);
            var serializer = new XmlSerializer(typeof(TOut));
            _ = stream.Seek(0, SeekOrigin.Begin);

            return (TOut)serializer.Deserialize(stream);
        }
    }
}
