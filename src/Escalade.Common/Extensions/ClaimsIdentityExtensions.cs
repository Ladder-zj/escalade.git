using System.Security.Claims;

namespace Escalade.Common
{
    /// <summary>
    /// ClaimsIdentity相关扩展
    /// </summary>
    public static class ClaimsIdentityExtensions
    {
        /// <summary>
        /// 查找首个值
        /// </summary>
        /// <param name="claimsIdentity">ClaimsIdentity</param>
        /// <param name="claimType">声明类型</param>
        /// <returns></returns>
        public static string FindFirstValue(this ClaimsIdentity claimsIdentity, string claimType)
        {
            if (claimsIdentity == null)
                return null;

            var claim = claimsIdentity.FindFirst(claimType);
            return claim?.Value;
        }
    }
}
