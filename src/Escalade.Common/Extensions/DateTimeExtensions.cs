using System;

namespace Escalade.Common
{
    /// <summary>
    /// DateTime相关扩展
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// 月份的第一天
        /// </summary>
        /// <param name="value">值</param>
        /// <returns>DateTime</returns>
        public static DateTime MonthFirstDayDate(this DateTime value)
        {
            return DateTime.Parse(value.ToString($"yyyy-MM-01"));
        }

        /// <summary>
        /// 月份的最后一天
        /// </summary>
        /// <param name="value">值</param>
        /// <returns>DateTime</returns>
        public static DateTime MonthLastDayDate(this DateTime value)
        {
            return DateTime.Parse(value.ToString($"yyyy-MM-01")).AddMonths(1).AddDays(-1);
        }

        /// <summary>
        /// 时间临界点
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static DateTime CriticalPoint(this DateTime value)
        {
            return value.Date.AddDays(1).AddMilliseconds(-1);
        }

        /// <summary>
        /// 时间临界点
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="criticalSecondsValue">临界秒值</param>
        /// <returns></returns>
        public static DateTime CriticalPoint(this DateTime value, double criticalSecondsValue)
        {
            if (criticalSecondsValue >= 0)
                criticalSecondsValue = -criticalSecondsValue;

            return value.Date.AddDays(1).AddSeconds(criticalSecondsValue);
        }

        /// <summary>
        /// 时间临界点
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="timeSpan">时段</param>
        /// <returns></returns>
        public static DateTime CriticalPoint(this DateTime value, TimeSpan timeSpan)
        {
            var totalMilliseconds = timeSpan.TotalMilliseconds;
            if (totalMilliseconds >= 0)
                totalMilliseconds = -totalMilliseconds;

            return value.Date.AddDays(1).AddMilliseconds(totalMilliseconds);
        }
    }
}
