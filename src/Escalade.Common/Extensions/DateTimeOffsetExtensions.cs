using System;

namespace Escalade.Common
{
    /// <summary>
    /// DateTimeOffset相关扩展
    /// </summary>
    public static class DateTimeOffsetExtensions
    {
        /// <summary>
        /// 月份的第一天
        /// </summary>
        /// <param name="value">值</param>
        /// <returns>DateTimeOffset</returns>
        public static DateTimeOffset MonthFirstDayDate(this DateTimeOffset value)
        {
            return DateTimeOffset.Parse(value.ToString($"yyyy-MM-01"));
        }

        /// <summary>
        /// 月份的最后一天
        /// </summary>
        /// <param name="value">值</param>
        /// <returns>DateTimeOffset</returns>
        public static DateTimeOffset MonthLastDayDate(this DateTimeOffset value)
        {
            return DateTimeOffset.Parse(value.ToString($"yyyy-MM-01")).AddMonths(1).AddDays(-1);
        }

        /// <summary>
        /// 时间临界点
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static DateTimeOffset CriticalPoint(this DateTimeOffset value)
        {
            return value.Date.AddDays(1).AddMilliseconds(-1);
        }

        /// <summary>
        /// 时间临界点
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="criticalSecondsValue">临界秒值</param>
        /// <returns></returns>
        public static DateTimeOffset CriticalPoint(this DateTimeOffset value, double criticalSecondsValue)
        {
            if (criticalSecondsValue >= 0)
                criticalSecondsValue = -criticalSecondsValue;

            return value.Date.AddDays(1).AddSeconds(criticalSecondsValue);
        }

        /// <summary>
        /// 时间临界点
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="timeSpan">时段</param>
        /// <returns></returns>
        public static DateTimeOffset CriticalPoint(this DateTimeOffset value, TimeSpan timeSpan)
        {
            var totalMilliseconds = timeSpan.TotalMilliseconds;
            if (totalMilliseconds >= 0)
                totalMilliseconds = -totalMilliseconds;

            return value.Date.AddDays(1).AddMilliseconds(totalMilliseconds);
        }
    }
}
