using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Escalade.Common
{
    /// <summary>
    /// Enum相关扩展
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// 获取枚举类型的所有成员值
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static IEnumerable<Enum> GetValues(this Enum value)
        {
            return Enum.GetValues(value.GetType()).Cast<Enum>();
        }

        /// <summary>
        /// 根据枚举成员的值获取其名称
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static string GetName(this Enum value)
        {
            return Enum.GetName(value.GetType(), value);
        }

        /// <summary>
        /// 获取枚举类型的所有成员名称
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static string[] GetNames(this Enum value)
        {
            return Enum.GetNames(value.GetType());
        }

        /// <summary>
        /// 获取枚举类型的所有成员名称集合
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static IEnumerable<string> GetNameList(this Enum value)
        {
            return Enum.GetNames(value.GetType()).ToList();
        }

        /// <summary>
        /// 获取枚举描述
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static string GetDescriptionValue(this Enum value)
        {
            return value.GetType().GetMember(value.ToString()).FirstOrDefault()?.GetCustomAttribute<DescriptionAttribute>()?.Description;
        }

        /// <summary>
        /// 获取枚举指定标记的Attribute
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static T GetAttributeValue<T>(this Enum value) where T : Attribute
        {
            var memberInfo = value.GetType().GetMember(value.ToString()).FirstOrDefault();
            if (memberInfo != null)
            {
                if (memberInfo.GetCustomAttributes(typeof(T), false).FirstOrDefault() is T attribute)
                {
                    return attribute;
                }
            }
            return null;
        }
    }
}
