using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;

namespace Escalade.Common
{
    /// <summary>
    /// Enumerable相关扩展
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// 数组是否有任意值
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static bool HasAnyValue<TSource>(this IEnumerable<TSource> value)
        {
            return value != null && value.Any();
        }

        /// <summary>
        /// 数组是否有任意值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="allowEmptyArray">允许为空数组</param>
        /// <returns></returns>
        public static bool HasAnyValue<TSource>(this IEnumerable<TSource> value, bool allowEmptyArray)
        {
            return value != null && (value.Any() || allowEmptyArray);
        }

        /// <summary>
        /// 数组是否有任意值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="predicate">predicate</param>
        /// <returns></returns>
        public static bool HasAnyValue<TSource>(this IEnumerable<TSource> value, Func<TSource, bool> predicate)
        {
            return value != null && value.Any(predicate);
        }

        /// <summary>
        /// 数组是否有任意值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="predicate">predicate</param>
        /// <param name="allowEmptyArray">允许为空数组</param>
        /// <returns></returns>
        public static bool HasAnyValue<TSource>(this IEnumerable<TSource> value, Func<TSource, bool> predicate, bool allowEmptyArray)
        {
            return value != null && (value.Any(predicate) || allowEmptyArray);
        }

        /// <summary>
        /// 获取首个元素数据源为NULL值时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static TSource GetFirstOrDefault<TSource>(this IEnumerable<TSource> value)
        {
            if (value == null)
                return default;

            return value.FirstOrDefault();
        }

        /// <summary>
        /// 获取首个元素数据源为NULL值时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static TSource GetFirstOrDefault<TSource>(this IEnumerable<TSource> value, TSource defaultValue = default)
        {
            if (value == null)
                return default;

            return value.FirstOrDefault() ?? defaultValue;
        }

        /// <summary>
        /// 获取首个元素数据源为NULL值时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="predicate">predicate</param>
        /// <returns></returns>
        public static TSource GetFirstOrDefault<TSource>(this IEnumerable<TSource> value, Func<TSource, bool> predicate)
        {
            if (value == null)
                return default;

            return value.FirstOrDefault(predicate);
        }

        /// <summary>
        /// 获取首个元素数据源为NULL值时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="predicate">predicate</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static TSource GetFirstOrDefault<TSource>(this IEnumerable<TSource> value, Func<TSource, bool> predicate, TSource defaultValue = default)
        {
            if (value == null)
                return default;

            return value.FirstOrDefault(predicate) ?? defaultValue;
        }

        /// <summary>
        /// 获取唯一元素数据源为NULL值时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static TSource GetSingleOrDefault<TSource>(this IEnumerable<TSource> value)
        {
            if (value == null)
                return default;

            return value.SingleOrDefault();
        }

        /// <summary>
        /// 获取唯一元素数据源为NULL值时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static TSource GetSingleOrDefault<TSource>(this IEnumerable<TSource> value, TSource defaultValue = default)
        {
            if (value == null)
                return default;

            return value.SingleOrDefault() ?? defaultValue;
        }

        /// <summary>
        /// 获取唯一元素数据源为NULL值时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="predicate">predicate</param>
        /// <returns></returns>
        public static TSource GetSingleOrDefault<TSource>(this IEnumerable<TSource> value, Func<TSource, bool> predicate)
        {
            if (value == null)
                return default;

            return value.SingleOrDefault(predicate);
        }

        /// <summary>
        /// 获取唯一元素数据源为NULL值时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="predicate">predicate</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static TSource GetSingleOrDefault<TSource>(this IEnumerable<TSource> value, Func<TSource, bool> predicate, TSource defaultValue = default)
        {
            if (value == null)
                return default;

            return value.SingleOrDefault(predicate) ?? defaultValue;
        }

        /// <summary>
        /// 尝试获取是否包含某值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="containValue">指定包含值</param>
        /// <returns></returns>
        public static bool TryContains<TSource>(this IEnumerable<TSource> value, TSource containValue)
        {
            if (value == null)
                return false;

            try
            {
                return value.Contains(containValue);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 尝试获取是否包含某值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="containValue">指定包含值</param>
        /// <param name="comparer">comparer</param>
        /// <returns></returns>
        public static bool TryContains<TSource>(this IEnumerable<TSource> value, TSource containValue, IEqualityComparer<TSource> comparer)
        {
            if (value == null)
                return false;

            try
            {
                return value.Contains(containValue, comparer);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 可控数据过滤
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="use">是否使用</param>
        /// <param name="predicate">过滤条件</param>
        /// <returns></returns>
        public static IEnumerable<TSource> WhereIF<TSource>(this IEnumerable<TSource> value, bool use, Func<TSource, bool> predicate)
        {
            if (use)
            {
                return value.Where(predicate);
            }

            return value;
        }

        /// <summary>
        /// 可控数据过滤
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="use">是否使用</param>
        /// <param name="predicate">过滤条件</param>
        /// <returns></returns>
        public static IEnumerable<TSource> WhereIF<TSource>(this IEnumerable<TSource> value, bool use, Func<TSource, int, bool> predicate)
        {
            if (use)
            {
                return value.Where(predicate);
            }

            return value;
        }

        /// <summary>
        /// 可控数据过滤
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="use">是否使用</param>
        /// <param name="expression">表达式</param>
        /// <returns></returns>
        public static IEnumerable<TSource> WhereIF<TSource>(this IEnumerable<TSource> value, bool use, Expression<Func<TSource, bool>> expression)
        {
            if (use)
            {
                return value.Where(expression.Compile());
            }

            return value;
        }

        /// <summary>
        /// 可控数据过滤
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="use">是否使用</param>
        /// <param name="expression">表达式</param>
        /// <returns></returns>
        public static IEnumerable<TSource> WhereIF<TSource>(this IEnumerable<TSource> value, bool use, Expression<Func<TSource, int, bool>> expression)
        {
            if (use)
            {
                return value.Where(expression.Compile());
            }

            return value;
        }

        /// <summary>
        /// 不为Null值
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static IEnumerable<TSource> NotNull<TSource>(this IEnumerable<TSource> value)
        {
            return value ?? Enumerable.Empty<TSource>();
        }

        /// <summary>
        /// 为Null时返回指定默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static IEnumerable<TSource> NotNull<TSource>(this IEnumerable<TSource> value, [NotNull] IEnumerable<TSource> defaultValue)
        {
            return value ?? defaultValue;
        }
    }
}
