using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Escalade.Common
{
    /// <summary>
    /// Genericity相关扩展
    /// </summary>
    public static class GenericityExtensions
    {
        /// <summary>
        /// 不为Null值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static TSoure NotNull<TSoure>(this TSoure value, [NotNull] TSoure defaultValue = default) where TSoure : class, new()
        {
            var type = typeof(TSoure);
            if (value != null)
                return value;
            else if (defaultValue != null)
                return defaultValue;
            else if (!type.CanCreateInstance())
                //抛出无法创建实例的异常
                throw new NotSupportedException("Cannot create an instance of CannotCreateInstanceClass.");
            else
                return Activator.CreateInstance<TSoure>();
        }

        /// <summary>
        /// 不为Null值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static TSoure TryNotNull<TSoure>(this TSoure value, [NotNull] TSoure defaultValue = default) where TSoure : class, new()
        {
            try
            {
                return value.NotNull(defaultValue);
            }
            catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// 是否为String类型
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static bool IsStringType<TSoure>(this TSoure value)
        {
            return value.GetType().Equals(typeof(string));
        }

        /// <summary>
        /// 是否不为Null值
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static bool IsNotNull<TSoure>(this TSoure value)
        {
            return value != null;
        }

        /// <summary>
        /// 获取值,为Null时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static TSoure GetValueOrDefault<TSoure>(this TSoure value, [NotNull] TSoure defaultValue = default)
        {
            return value ?? defaultValue;
        }

        /// <summary>
        /// 序列化成byte[]
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static byte[] SerializeToBytes<TSoure>(this TSoure value)
        {
            var serializer = new XmlSerializer(typeof(TSoure));

            using var stream = new MemoryStream();
            serializer.Serialize(stream, value);
            _ = stream.Seek(0, SeekOrigin.Begin);
            return stream.ToBytes();
        }

        /// <summary>
        /// 序列化成String
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static string SerializeToString<TSoure>(this TSoure value)
        {
            return JsonConvert.SerializeObject(value);
        }

        /// <summary>
        /// 序列化成String
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="settings">序列化设置</param>
        /// <returns></returns>
        public static string SerializeToString<TSoure>(this TSoure value, JsonSerializerSettings? settings)
        {
            return JsonConvert.SerializeObject(value, settings);
        }
    }
}
