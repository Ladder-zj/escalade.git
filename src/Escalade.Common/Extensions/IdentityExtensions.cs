using System;
using System.Security.Claims;
using System.Security.Principal;

namespace Escalade.Common
{
    /// <summary>
    /// Identity相关扩展
    /// </summary>
    public static class IdentityExtensions
    {
        /// <summary>
        /// 获取首个值
        /// </summary>
        /// <param name="identity">IIdentity</param>
        /// <param name="claimType">声明类型</param>
        /// <returns></returns>
        public static string GetFirstValue(this IIdentity identity, string claimType)
        {
            if (identity == null)
                return null;

            var claimsIdentity = identity as ClaimsIdentity;
            return claimsIdentity?.FindFirstValue(claimType);
        }

        /// <summary>
        /// 获取首个值为Null时返回默认值
        /// </summary>
        /// <param name="identity">IIdentity</param>
        /// <param name="claimType">声明类型</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static string GetFirstOrDefaultValue(this IIdentity identity, string claimType, string defaultValue)
        {
            if (identity == null)
                return defaultValue;

            var claimsIdentity = identity as ClaimsIdentity;
            return claimsIdentity?.FindFirstValue(claimType) ?? defaultValue;
        }

        /// <summary>
        /// 获取首个值为Null时返回默认值
        /// </summary>
        /// <param name="identity">IIdentity</param>
        /// <param name="claimType">声明类型</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static TOut GetFirstOrDefaultValue<TOut>(this IIdentity identity, string claimType, TOut defaultValue = default)
        {
            if (identity == null)
                return defaultValue;

            var claimsIdentity = identity as ClaimsIdentity;
            var value = claimsIdentity?.FindFirstValue(claimType);
            if (value == null)
                return defaultValue;

            try
            {
                return (TOut)Convert.ChangeType(value, typeof(TOut));
            }
            catch (InvalidCastException)
            {
                throw;
            }
        }
    }
}
