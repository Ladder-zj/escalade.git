using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Escalade.Common
{
    /// <summary>
    /// Object相关扩展
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// 获取值,为Null时返回默认值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static object GetValueOrDefault(this object value, object defaultValue)
        {
            return value ?? defaultValue;
        }

        /// <summary>
        /// 获取描述
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static string GetDescriptionValue(this object value)
        {
            return value.GetType().GetMember(value.ToString() ?? string.Empty).FirstOrDefault()?.GetCustomAttribute<DescriptionAttribute>()?.Description;
        }
    }
}