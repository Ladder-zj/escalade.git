using System;
using Microsoft.Extensions.DependencyInjection;

namespace Escalade.Common
{
    /// <summary>
    /// ServiceCollection相关扩展
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 添加SecureUtil
        /// </summary>
        /// <param name="services">IServiceCollection</param>
        /// <param name="options">SecureUtil选项</param>
        /// <returns></returns>
        public static IServiceCollection AddSecure(this IServiceCollection services, Action<SecureUtil> options)
        {
            var secureUtil = new SecureUtil();
            options.Invoke(secureUtil);
            _ = services.AddSingleton(secureUtil);
            return services;
        }

        /// <summary>
        /// 添加IdWorkerUtil
        /// </summary>
        /// <param name="services">IServiceCollection</param>
        /// <param name="workerId">WorkerId</param>
        /// <returns></returns>
        public static IServiceCollection AddIdWorker(this IServiceCollection services, long workerId)
        {
            return services.AddIdWorker(workerId, null);
        }

        /// <summary>
        /// 添加IdWorkerUtil
        /// </summary>
        /// <param name="services">IServiceCollection</param>
        /// <param name="workerId">WorkerId</param>
        /// <param name="customNextIdFunc">自定义生成规则</param>
        /// <returns></returns>
        public static IServiceCollection AddIdWorker(this IServiceCollection services, long workerId, Func<long> customNextIdFunc)
        {
            IdWorkerUtil.WorkerId = workerId;
            if (customNextIdFunc != null)
                IdWorkerUtil.CustomNextIdFunc = customNextIdFunc;

            return services;
        }
    }
}
