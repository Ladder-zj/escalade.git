using System;
using System.IO;

namespace Escalade.Common
{
    /// <summary>
    /// Stream相关扩展
    /// </summary>
    public static class StreamExtensions
    {
        /// <summary>
        /// 转换成byte[]
        /// </summary>
        /// <param name="stream">值</param>
        /// <returns></returns>
        public static byte[] ToBytes(this Stream stream)
        {
            var bytes = new byte[stream.Length];
            _ = stream.Read(bytes, 0, bytes.Length);
            _ = stream.Seek(0, SeekOrigin.Begin);
            return bytes;
        }

        /// <summary>
        /// 转换成byte[]
        /// </summary>
        /// <param name="stream">值</param>
        /// <param name="count">当前流中读取的最大字节数</param>
        /// <returns></returns>
        public static byte[] ToBytes(this Stream stream, int count)
        {
            var buffer = new byte[count];
            var bytesRead = stream.Read(buffer, 0, buffer.Length);
            if (bytesRead != count)
            {
                Array.Resize(ref buffer, bytesRead);
            }
            return buffer;
        }

        /// <summary>
        /// 从流中读取文本数据
        /// </summary>
        /// <param name="stream">值</param>
        /// <returns></returns>
        public static string ReadString(this Stream stream)
        {
            using var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }
    }
}
