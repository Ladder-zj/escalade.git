using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Escalade.Common
{
    /// <summary>
    /// String相关扩展
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// 尝试获取是否包含某值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="containValue">指定包含值</param>
        /// <returns></returns>
        public static bool TryContains(this string value, char containValue)
        {
            if (value == null)
                return false;

            try
            {
                return value.Contains(containValue);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 尝试获取是否包含某值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="containValue">指定包含值</param>
        /// <param name="comparisonType">比较类型</param>
        /// <returns></returns>
        public static bool TryContains(this string value, char containValue, StringComparison comparisonType)
        {
            if (value == null)
                return false;

            try
            {
                return value.Contains(containValue, comparisonType);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 尝试获取是否包含某值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="containValue">指定包含值</param>
        /// <returns></returns>
        public static bool TryContains(this string value, string containValue)
        {
            if (value == null)
                return false;

            try
            {
                return value.Contains(containValue);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 尝试获取是否包含某值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="containValue">指定包含值</param>
        /// <param name="comparisonType">比较类型</param>
        /// <returns></returns>
        public static bool TryContains(this string value, string containValue, StringComparison comparisonType)
        {
            if (value == null)
                return false;

            try
            {
                return value.Contains(containValue, comparisonType);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 处理值一定不为Null
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static string NotNull(this string value, [NotNull] string defaultValue = "")
        {
            return value ?? defaultValue;
        }

        /// <summary>
        /// 十六进制字符转换成byte[]
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static byte[] HexStringToByte(this string value)
        {
            return Enumerable.Range(0, value.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(value.Substring(x, 2), 16))
                             .ToArray();
        }

        /// <summary>
        /// 尝试转换十六进制字符成byte[]
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="defaultValue">指定默认值</param>
        /// <returns></returns>
        public static byte[] TryHexStringToByte(this string value, byte[] defaultValue = default)
        {
            try
            {
                return HexStringToByte(value);
            }
            catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// 转换成byte[],默认编码格式
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static byte[] ToBytes(this string value)
        {
            return Encoding.Default.GetBytes(value);
        }

        /// <summary>
        /// 转换成byte[],默认编码格式
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="index">起始索引，表示要转换的字符串的起始位置</param>
        /// <param name="count">要转换的字符数，表示要转换的字符串的长度</param>
        /// <returns></returns>
        public static byte[] ToBytes(this string value, int index, int count)
        {
            return Encoding.Default.GetBytes(value, index, count);
        }

        /// <summary>
        /// 转换成byte[],UTF8编码格式
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static byte[] ToUTF8Bytes(this string value)
        {
            return Encoding.UTF8.GetBytes(value);
        }

        /// <summary>
        /// 转换成byte[],UTF8编码格式
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="index">起始索引，表示要转换的字符串的起始位置</param>
        /// <param name="count">要转换的字符数，表示要转换的字符串的长度</param>
        /// <returns></returns>
        public static byte[] ToUTF8Bytes(this string value, int index, int count)
        {
            return Encoding.UTF8.GetBytes(value, index, count);
        }

        /// <summary>
        /// Base64编码字符串转换成byte[]
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] ToBytesFromBase64String(this string value)
        {
            return Convert.FromBase64String(value);
        }

        /// <summary>
        /// MD5使用StringBuilder方式
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        [Obsolete]
        public static string MD5StringBuilder(this string value)
        {
            return SecurityUtil.MD5StringBuilder(value);
        }

        /// <summary>
        /// MD5使用BitConverter方式
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        [Obsolete]
        public static string MD5BitConverter(this string value)
        {
            return SecurityUtil.MD5BitConverter(value);
        }

        /// <summary>
        /// MD5使用StringConcat方式
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        [Obsolete]
        public static string MD5StringConcat(this string value)
        {
            return SecurityUtil.MD5StringConcat(value);
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="aesKey">AES密钥</param>
        /// <param name="aesIv">AES初始化向量</param>
        /// <returns></returns>
        [Obsolete]
        public static string AESEncrypt(this string value, string aesKey, string aesIv)
        {
            return SecurityUtil.AESEncrypt(value, aesKey, aesIv);
        }

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="aesKey">AES密钥</param>
        /// <param name="aesIv">AES初始化向量</param>
        /// <returns></returns>
        [Obsolete]
        public static string AESDecrypt(this string value, string aesKey, string aesIv)
        {
            return SecurityUtil.AESDecrypt(value, aesKey, aesIv);
        }

        /// <summary>
        /// RSA加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="publicKey">公钥</param>
        /// <returns></returns>
        [Obsolete]
        public static string RSAEncrypt(this string value, string publicKey)
        {
            return SecurityUtil.RSAEncrypt(value, publicKey);
        }

        /// <summary>
        /// RSA解密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="privateKey">私钥</param>
        /// <returns></returns>
        [Obsolete]
        public static string RSADecrypt(this string value, string privateKey)
        {
            return SecurityUtil.RSADecrypt(value, privateKey);
        }

        /// <summary>
        /// 使用哈希算法加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="hashAlgorithmType">哈希算法类型(default:MD5)</param>
        /// <returns></returns>
        public static string EncryptByHashAlgorithm(this string value, HashAlgorithmType hashAlgorithmType = HashAlgorithmType.MD5)
        {
            return SecureUtil.EncryptByHashAlgorithm(value, hashAlgorithmType);
        }

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string DESEncrypt(this string value, DESSetting setting)
        {
            return SecureUtil.DESEncrypt(value, setting);
        }

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="secretKey">密钥</param>
        /// <returns></returns>
        public static string DESEncrypt(this string value, string secretKey)
        {
            return SecureUtil.DESEncrypt(value, new DESSetting() { SecretKey = secretKey, ModeType = SecureCipherModeType.ECB });
        }

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="secretKey">密钥</param>
        /// <param name="iv">初始化向量</param>
        /// <returns></returns>
        public static string DESEncrypt(this string value, string secretKey, string iv)
        {
            return SecureUtil.DESEncrypt(value, new DESSetting() { SecretKey = secretKey, IV = iv });
        }

        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string DESDecrypt(this string value, DESSetting setting)
        {
            return SecureUtil.DESDecrypt(value, setting);
        }

        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="secretKey">密钥</param>
        /// <returns></returns>
        public static string DESDecrypt(this string value, string secretKey)
        {
            return SecureUtil.DESDecrypt(value, new DESSetting() { SecretKey = secretKey, ModeType = SecureCipherModeType.ECB });
        }

        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="secretKey">密钥</param>
        /// <param name="iv">初始化向量</param>
        /// <returns></returns>
        public static string DESDecrypt(this string value, string secretKey, string iv)
        {
            return SecureUtil.DESDecrypt(value, new DESSetting() { SecretKey = secretKey, IV = iv });
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string AESEncrypt(this string value, AESSetting setting)
        {
            return SecureUtil.AESEncrypt(value, setting);
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="secretKey">密钥</param>
        /// <returns></returns>
        public static string AESEncrypt(this string value, string secretKey)
        {
            return SecureUtil.AESEncrypt(value, new AESSetting() { SecretKey = secretKey, ModeType = SecureCipherModeType.ECB });
        }

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string AESDecrypt(this string value, AESSetting setting)
        {
            return SecureUtil.AESDecrypt(value, setting);
        }

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="secretKey">密钥</param>
        /// <returns></returns>
        public static string AESDecrypt(this string value, string secretKey)
        {
            return SecureUtil.AESDecrypt(value, new AESSetting() { SecretKey = secretKey, ModeType = SecureCipherModeType.ECB });
        }

        /// <summary>
        /// RSA加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string RSAEncrypt(this string value, RSASetting setting)
        {
            return SecureUtil.RSAEncrypt(value, setting);
        }

        /// <summary>
        /// RSA公钥加密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="publicKey">公钥</param>
        /// <returns></returns>
        public static string RSAEncryptByKey(this string value, string publicKey)
        {
            return SecureUtil.RSAEncrypt(value, new RSASetting() { PublicKey = publicKey });
        }

        /// <summary>
        /// RSA解密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string RSADecrypt(this string value, RSASetting setting)
        {
            return SecureUtil.RSADecrypt(value, setting);
        }

        /// <summary>
        /// RSA私钥解密
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="privateKey">私钥</param>
        /// <returns></returns>
        public static string RSADecryptByKey(this string value, string privateKey)
        {
            return SecureUtil.RSADecrypt(value, new RSASetting() { PrivateKey = privateKey });
        }

        /// <summary>
        /// 限制字符长度，超出时截取
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="limit">限制字符长度</param>
        /// <returns></returns>
        public static string Limit(this string value, int limit)
        {
            return value.Length > limit ? value[..limit] : value;
        }

        /// <summary>
        /// 限制字符长度，超出时截取
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="limit">限制字符长度</param>
        /// <param name="startIndex">起始索引</param>
        /// <returns></returns>
        public static string Limit(this string value, int limit, int startIndex)
        {
            if (startIndex > 0)
                value = value[startIndex..];

            return value.Limit(limit);
        }

        /// <summary>
        /// 限制字符长度，超出时截取并在之后追加指定字符值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="limit">限制字符长度</param>
        /// <param name="appendValue">指定追加字符值</param>
        /// <returns></returns>
        public static string LimitAppendAfter(this string value, int limit, [NotNull] string appendValue)
        {
            AppendValueValid(appendValue);

            return value.Limit(limit) + appendValue;
        }

        /// <summary>
        /// 限制字符长度，超出时截取并在之后追加指定字符值
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="limit">限制字符长度</param>
        /// <param name="startIndex">起始索引</param>
        /// <param name="appendValue">指定追加字符值</param>
        /// <returns></returns>
        public static string LimitAppendAfter(this string value, int limit, int startIndex, [NotNull] string appendValue)
        {
            AppendValueValid(appendValue);

            return value.Limit(limit, startIndex) + appendValue;
        }

        /// <summary>
        /// 验证追加字符参数有效性
        /// </summary>
        /// <param name="appendValue">追加的字符</param>
        private static void AppendValueValid(string appendValue)
        {
            if (string.IsNullOrWhiteSpace(appendValue))
                throw new ArgumentException("追加的字符不能是null、空或仅由空白组成", nameof(appendValue));
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static TOut DeserializeObject<TOut>(this string value)
        {
            return JsonConvert.DeserializeObject<TOut>(value);
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="settings">序列化设置</param>
        /// <returns></returns>
        public static TOut DeserializeObject<TOut>(this string value, JsonSerializerSettings? settings)
        {
            return JsonConvert.DeserializeObject<TOut>(value, settings);
        }

        /// <summary>
        /// 可控追加字符
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="use">是否追加</param>
        /// <param name="appendValue">追加值</param>
        /// <param name="appendValueWayType">追加方式(default:之后)</param>
        /// <returns></returns>
        public static string AppendIF(this string value, bool use, string appendValue, AppendValueWayType appendValueWayType = AppendValueWayType.After)
        {
            return use ? value.Append(appendValue, appendValueWayType) : value;
        }

        /// <summary>
        /// 可控追加字符
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="use">是否追加</param>
        /// <param name="appendValueWayType">追加方式</param>
        /// <param name="appendValues">追加值</param>
        /// <returns></returns>
        public static string AppendIF(this string value, bool use, AppendValueWayType appendValueWayType, params string[] appendValues)
        {
            return value.AppendIF(use, appendValues.ToList(), appendValueWayType);
        }

        /// <summary>
        /// 可控追加字符
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="use">是否追加</param>
        /// <param name="appendValues">追加值</param>
        /// <param name="appendValueWayType">追加方式(default:之后)</param>
        /// <returns></returns>
        public static string AppendIF(this string value, bool use, IList<string> appendValues, AppendValueWayType appendValueWayType = AppendValueWayType.After)
        {
            return use ? value.Append(appendValues, appendValueWayType) : value;
        }

        /// <summary>
        /// 追加字符
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="appendValue">追加值</param>
        /// <param name="appendValueWayType">追加方式(default:之后)</param>
        /// <returns></returns>
        public static string Append(this string value, string appendValue, AppendValueWayType appendValueWayType = AppendValueWayType.After)
        {
            appendValue = appendValue.NotNull();
            if (appendValueWayType == AppendValueWayType.After)
                return value + appendValue;
            else
                return appendValue + value;
        }

        /// <summary>
        /// 追加字符
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="appendValueWayType">追加方式</param>
        /// <param name="appendValues">追加值</param>
        /// <returns></returns>
        public static string Append(this string value, AppendValueWayType appendValueWayType, params string[] appendValues)
        {
            return value.Append(appendValues.ToList(), appendValueWayType);
        }

        /// <summary>
        /// 追加字符
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="appendValues">追加值</param>
        /// <param name="appendValueWayType">追加方式(default:之后)</param>
        /// <returns></returns>
        public static string Append(this string value, IList<string> appendValues, AppendValueWayType appendValueWayType = AppendValueWayType.After)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < appendValues.Count; i++)
            {
                _ = sb.Append(appendValues[i].NotNull());
            }

            return value.Append(sb.ToString(), appendValueWayType);
        }
    }
}
