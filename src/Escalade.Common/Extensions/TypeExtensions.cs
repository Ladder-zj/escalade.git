using System;
using System.Collections.Generic;
using System.Linq;

namespace Escalade.Common
{
    /// <summary>
    /// Type相关扩展
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// 是否可以创建实例
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static bool CanCreateInstance(this Type value)
        {
            if (value.IsAbstract || value.IsInterface || value.IsGenericTypeDefinition)
            {
                return false;
            }

            if (value.IsValueType || value == typeof(string))
            {
                return true;
            }

            return value.GetConstructor(Type.EmptyTypes) != null;
        }

        /// <summary>
        /// 是否为集合类型
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static bool IsCollection(this Type value)
        {
            return value?.GetInterfaces().HasAnyValue(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>)) ?? false;
        }

        /// <summary>
        /// 是否标记指定特性
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static bool HasAttribute<T>(this Type value) where T : Attribute
        {
            return value?.GetCustomAttributes(typeof(T), true).HasAnyValue() ?? false;
        }

        /// <summary>
        /// 获取指定特性
        /// </summary>
        /// <param name="value">值</param>
        /// <returns></returns>
        public static T GetAttribute<T>(this Type value) where T : Attribute
        {
            return (T)value?.GetCustomAttributes(typeof(T), true).FirstOrDefault();
        }
    }
}
