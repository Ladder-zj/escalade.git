using System.Collections;

namespace Escalade.Common
{
    /// <summary>
    /// 树结构组成接口
    /// </summary>
    public interface ITreeComponent
    {
        /// <summary>
        /// 获取Id
        /// </summary>
        /// <returns></returns>
        long GetId();

        /// <summary>
        /// 获取父级Id
        /// </summary>
        /// <returns></returns>
        long GetParentId();

        /// <summary>
        /// 赋值子项
        /// </summary>
        /// <param name="children">子项数据</param>
        void SetChildren(IList children);
    }
}
