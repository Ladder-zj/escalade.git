namespace Escalade.Common
{
    /// <summary>
    /// AES加解密配置
    /// </summary>
    public class AESSetting
    {
        /// <summary>
        /// 初始化向量
        /// </summary>
        public string IV { get; set; }

        /// <summary>
        /// 私钥
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// KeySize(default:256) 有效值为:128、160、192、224或256
        /// </summary>
        public int KeySize { get; set; } = 256;

        /// <summary>
        /// 加解密模式类型(default:CBC模式)
        /// </summary>
        public SecureCipherModeType ModeType { get; set; } = SecureCipherModeType.CBC;
    }
}
