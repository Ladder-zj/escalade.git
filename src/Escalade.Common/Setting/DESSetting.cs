namespace Escalade.Common
{
    /// <summary>
    /// DES加解密配置
    /// </summary>
    public class DESSetting
    {
        /// <summary>
        /// 初始化向量
        /// </summary>
        public string IV { get; set; }

        /// <summary>
        /// 秘钥
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// 加解密模式类型(default:CBC模式)
        /// </summary>
        public SecureCipherModeType ModeType { get; set; } = SecureCipherModeType.CBC;
    }
}
