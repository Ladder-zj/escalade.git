namespace Escalade.Common
{
    /// <summary>
    /// RSA加解密配置
    /// </summary>
    public class RSASetting
    {
        /// <summary>
        /// 公钥
        /// </summary>
        public string PublicKey { get; set; }

        /// <summary>
        /// 私钥
        /// </summary>
        public string PrivateKey { get; set; }

        /// <summary>
        /// 生成格式类型(default:PKCS8)
        /// </summary>
        public RSAGenFormatType GenFormatType { get; set; } = RSAGenFormatType.PKCS8;
    }
}
