using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Escalade.Common
{
    /// <summary>
    /// IP相关Util
    /// </summary>
    public static class IPUtil
    {
        /// <summary>
        /// 获取本机公网地址
        /// </summary>
        /// <returns></returns>
        public static string GetPublicIP()
        {
            return new WebClient().DownloadString("http://icanhazip.com").Replace("\n", string.Empty).Replace("\r", string.Empty).Trim();
        }

        /// <summary>
        /// 获取本机内网地址
        /// </summary>
        /// <param name="addressFamily">指定寻址方案(default:IPv4)</param>
        /// <returns></returns>
        public static string GetLocalIP(AddressFamily addressFamily = AddressFamily.InterNetwork)
        {
            return Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == addressFamily).ToString();
        }

        /// <summary>
        /// 获取本机无线局域网地址
        /// </summary>
        /// <param name="addressFamily">指定寻址方案(default:IPv4)</param>
        /// <returns></returns>
        public static string GetLocalWalnIP(AddressFamily addressFamily = AddressFamily.InterNetwork)
        {
            var networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();

            var wifiInterface = networkInterfaces.FirstOrDefault(
                nic => nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 && nic.OperationalStatus == OperationalStatus.Up);

            if (wifiInterface != null)
            {
                var ipProperties = wifiInterface.GetIPProperties();

                var ipv4Address = ipProperties.UnicastAddresses
                    .FirstOrDefault(addr => addr.Address.AddressFamily == addressFamily)?.Address;

                return ipv4Address?.ToString();
            }

            return null;
        }

        /// <summary>
        /// 获取本机公网IPAddress实例
        /// </summary>
        /// <returns></returns>
        public static IPAddress GetPublicIPAddress()
        {
            return IPAddress.Parse(GetPublicIP());
        }

        /// <summary>
        /// 获取本机内网IPAddress实例
        /// </summary>
        /// <param name="addressFamily">指定寻址方案(default:IPv4)</param>
        /// <returns></returns>
        public static IPAddress GetLocalIPAddress(AddressFamily addressFamily = AddressFamily.InterNetwork)
        {
            return IPAddress.Parse(GetLocalIP(addressFamily));
        }

        /// <summary>
        /// 获取本机无线局域网IPAddress实例
        /// </summary>
        /// <param name="addressFamily">指定寻址方案(default:IPv4)</param>
        /// <returns></returns>
        public static IPAddress GetLocalWalnIPAddress(AddressFamily addressFamily = AddressFamily.InterNetwork)
        {
            return IPAddress.Parse(GetLocalWalnIP(addressFamily) ?? string.Empty);
        }

        /// <summary>
        /// 比较IP是否相同
        /// </summary>
        /// <param name="ip1">ip1</param>
        /// <param name="ip2">ip2</param>
        /// <returns></returns>
        public static bool IPEquals(string ip1, string ip2)
        {
            var ipAddress1 = IPAddress.Parse(ip1);
            var ipAddress2 = IPAddress.Parse(ip2);

            return ipAddress1.Equals(ipAddress2) || ipAddress1 == ipAddress2;
        }

        /// <summary>
        /// 比较IP是否相同
        /// </summary>
        /// <param name="iPAddress1">iPAddress1</param>
        /// <param name="iPAddress2">iPAddress2</param>
        /// <returns></returns>
        public static bool IPEquals(IPAddress iPAddress1, IPAddress iPAddress2)
        {
            return iPAddress1.Equals(iPAddress2);
        }

        /// <summary>
        /// 是否为IPv4地址
        /// </summary>
        /// <param name="ipAddress">ip地址</param>
        /// <returns></returns>
        public static bool IsIPv4(string ipAddress)
        {
            return IPAddress.Parse(ipAddress).AddressFamily == AddressFamily.InterNetwork;
        }

        /// <summary>
        /// 是否为IPv6地址
        /// </summary>
        /// <param name="ipAddress">ip地址</param>
        /// <returns></returns>
        public static bool IsIPv6(string ipAddress)
        {
            return IPAddress.Parse(ipAddress).AddressFamily == AddressFamily.InterNetworkV6;
        }
    }
}
