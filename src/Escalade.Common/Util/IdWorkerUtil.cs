using System;

namespace Escalade.Common
{
    /// <summary>
    /// IdWorker相关Util
    /// </summary>
    public static class IdWorkerUtil
    {
        /// <summary>
        /// WorkerId，应小于32
        /// </summary>
        public static long WorkerId { get; set; }

        /// <summary>
        /// 系统上线时间,默认取2023-01-01 00:00:00
        /// </summary>
        public static DateTime SystemLaunchTime { get; set; } = new DateTime(2023, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// 自定义NextId方法生成
        /// </summary>
        public static Func<long> CustomNextIdFunc { get; set; }

        /// <summary>
        /// 自增序列号
        /// </summary>
        private static long sequence = 0L;

        /// <summary>
        /// workerId Bit
        /// </summary>
        private static readonly int workerIdBits = 10;

        /// <summary>
        /// WorkerId最大值
        /// </summary>
        private static readonly long maxWorkerId = -1L ^ (-1L << workerIdBits);

        /// <summary>
        /// 序列号bit
        /// </summary>
        private static readonly int sequenceBits = 8;

        /// <summary>
        /// workerId左移位数
        /// </summary>
        private static readonly int workerIdLeftShift = 8;

        /// <summary>
        /// idType左移位数
        /// </summary>
        private static readonly int idTypeLeftShift = 8 + 10;

        /// <summary>
        /// 时间戳左移位数
        /// </summary>
        private static readonly int timestampLeftShift = 8 + 10 + 4;

        /// <summary>
        /// 防止溢出
        /// </summary>
        private static readonly long sequenceMask = -1L ^ (-1L << sequenceBits);

        /// <summary>
        /// lastTimestamp
        /// </summary>
        private static long lastTimestamp = -1L;

        /// <summary>
        /// 同步锁
        /// </summary>
        private static readonly object syncRoot = new object();

        /// <summary>
        /// 生成唯一Id
        /// </summary>
        /// <param name="idType"></param>
        /// <returns></returns>
        public static long NextId(int idType = 0)
        {
            ParamsValid();

            if (CustomNextIdFunc != null)
                return CustomNextIdFunc.Invoke();

            lock (syncRoot)
            {
                var timestamp = TimeGen();
                if (timestamp < lastTimestamp)
                {
                    throw new ApplicationException($"Clock moved backwards.  Refusing to generate id for {lastTimestamp - timestamp} milliseconds");
                }
                //如果为同一毫秒内生成，则 ++sequence 或 重新生成 
                if (lastTimestamp == timestamp)
                {
                    sequence = (sequence + 1) & sequenceMask;
                    if (sequence == 0)
                    {
                        timestamp = TilNextMillis(lastTimestamp);
                    }
                }
                else
                {
                    sequence = 0L;
                }

                lastTimestamp = timestamp;
#pragma warning disable CS0675 // 对进行了带符号扩展的操作数使用了按位或运算符
                return (timestamp << timestampLeftShift) | (idType << idTypeLeftShift) | (WorkerId << workerIdLeftShift) | sequence;
#pragma warning restore CS0675 // 对进行了带符号扩展的操作数使用了按位或运算符
            }
        }

        /// <summary>
        /// 验证参数有效性
        /// </summary>
        private static void ParamsValid()
        {
            if (WorkerId > maxWorkerId || WorkerId < 0)
            {
                throw new ArgumentException($"worker Id can't be greater than {maxWorkerId} or less than 0");
            }
        }

        /// <summary>
        /// 刷新时间戳
        /// </summary>
        /// <param name="lastTimestamp">lastTimestamp</param>
        /// <returns></returns>
        private static long TilNextMillis(long lastTimestamp)
        {
            var timestamp = TimeGen();
            while (timestamp <= lastTimestamp)
            {
                timestamp = TimeGen();
            }
            return timestamp;
        }

        /// <summary>
        /// 获取毫秒级时间戳
        /// </summary>
        /// <returns></returns>
        private static long TimeGen() => (long)(DateTime.UtcNow - SystemLaunchTime).TotalMilliseconds;
    }
}
