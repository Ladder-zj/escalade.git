using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using EasyTool.Extension;
using Org.BouncyCastle.Utilities.IO.Pem;

namespace Escalade.Common
{
    /// <summary>
    /// 哈希算法、加解密等维持安全相关Util
    /// </summary>
    public sealed partial class SecureUtil
    {
        /// <summary>
        /// DES加解密配置
        /// </summary>
        public DESSetting DESSetting { get; set; }

        /// <summary>
        /// AES加解密配置
        /// </summary>
        public AESSetting AESSetting { get; set; }

        /// <summary>
        /// RSA加解密配置
        /// </summary>
        public RSASetting RSASetting { get; set; }

        /// <summary>
        /// KeySize可选范围
        /// </summary>
        private static readonly List<int> KeySizeRange = new List<int>() { 168, 160, 192, 224, 256 };

        /// <summary>
        /// 生成对称算法密钥
        /// </summary>
        /// <param name="symmetricAlgorithmType">对称算法类型</param>
        /// <param name="cipherModeType">加解密模式类型(default:CBC模式)</param>
        /// <param name="keySize">key的大小(default:256) 有效值为:128、160、192、224或256</param>
        /// <returns>(密钥,IV向量)</returns>
        private static (string, string?) GenerateSymmetricAlgorithmKey(SymmetricAlgorithmType symmetricAlgorithmType, SecureCipherModeType cipherModeType = SecureCipherModeType.CBC, int keySize = 256)
        {
            SymmetricAlgorithm sa;
            if (symmetricAlgorithmType == SymmetricAlgorithmType.DES)
            {
                sa = DES.Create();
            }
            else
            {
                sa = Aes.Create();
                sa.KeySize = keySize;
            }
                
            //ECB模式无需IV向量
            sa.Mode = cipherModeType == SecureCipherModeType.CBC ? CipherMode.CBC : CipherMode.ECB;
            sa.Padding = PaddingMode.PKCS7;

            string iv = null;
            sa.GenerateKey();
            if (cipherModeType == SecureCipherModeType.CBC)
            {
                sa.GenerateIV();
                iv = sa.IV.ToBase64String();
            }

            sa.Dispose();
            return (sa.Key.ToBase64String(), iv);
        }

        #region 创建加解密对象
        /// <summary>
        /// 创建DES算法操作对象
        /// </summary>
        /// <param name="setting">配置</param>
        /// <returns></returns>
        private static DESCryptoServiceProvider CreateDESCryptoServiceProvider(DESSetting setting)
        {
            _ = DESSettingValid(setting);
            var des = new DESCryptoServiceProvider();

            //秘钥数组
            var secretBytes = Encoding.UTF8.GetBytes(setting.SecretKey);
            var keyBytes = new byte[8];
            Array.Copy(secretBytes, keyBytes, Math.Min(secretBytes.Length, keyBytes.Length));
            des.Key = keyBytes;

            if (setting.ModeType == SecureCipherModeType.CBC)
            {
                //初始向量数组
                var array = Encoding.UTF8.GetBytes(setting.IV);
                var ivBytes = new byte[keyBytes.Length];
                Array.Copy(array, ivBytes, Math.Min(array.Length, ivBytes.Length));
                des.IV = ivBytes;

                des.Mode = CipherMode.CBC;//CBC模式
            }
            else
            {
                des.Mode = CipherMode.ECB;//ECB模式
            }

            des.Padding = PaddingMode.PKCS7;//填充方式
            return des;
        }

        /// <summary>
        /// 创建AES算法操作对象
        /// </summary>
        /// <param name="setting">配置</param>
        /// <returns></returns>
        private static RijndaelManaged CreateRijndaelManaged(AESSetting setting)
        {
            _ = AESSettingValid(setting);

            var rijndaelManaged = new RijndaelManaged
            {
                Padding = PaddingMode.PKCS7,
                KeySize = setting.KeySize,
                BlockSize = 128,//必须为128
                Mode = setting.ModeType == SecureCipherModeType.CBC ? CipherMode.CBC : CipherMode.ECB
            };

            var secretBytes = Encoding.UTF8.GetBytes(setting.SecretKey);
            var keyBytes = new byte[16];
            Array.Copy(secretBytes, keyBytes, Math.Min(secretBytes.Length, keyBytes.Length));
            rijndaelManaged.Key = keyBytes;

            if (setting.ModeType == SecureCipherModeType.CBC)
            {
                var array = Encoding.UTF8.GetBytes(setting.IV);
                var ivBytes = new byte[keyBytes.Length];
                Array.Copy(array, ivBytes, Math.Min(array.Length, ivBytes.Length));
                rijndaelManaged.IV = ivBytes;
            }

            return rijndaelManaged;
        }

        /// <summary>
        /// 创建RSA算法操作对象
        /// </summary>
        /// <param name="setting"></param>
        /// <param name="asymmetricSecretKeyType"></param>
        /// <returns></returns>
        public static RSACryptoServiceProvider CreateRSACryptoServiceProvider(RSASetting setting, AsymmetricSecretKeyType asymmetricSecretKeyType)
        {
            _ = RSASettingValid(setting, asymmetricSecretKeyType);

            var rsa = new RSACryptoServiceProvider();
            if (asymmetricSecretKeyType == AsymmetricSecretKeyType.PrivateKey)
            {
                if (setting.GenFormatType == RSAGenFormatType.PKCS8)
                    rsa.ImportPkcs8PrivateKey(setting.PrivateKey.ToBytesFromBase64String(), out _);
                else
                    rsa.ImportRSAPrivateKey(setting.PrivateKey.ToBytesFromBase64String(), out _);
            }
            else
            {
                if (setting.GenFormatType == RSAGenFormatType.PKCS8)
                    rsa.ImportSubjectPublicKeyInfo(setting.PublicKey.ToBytesFromBase64String(), out _);
                else
                    rsa.ImportRSAPublicKey(setting.PublicKey.ToBytesFromBase64String(), out _);
            }
            return rsa;
        }
        #endregion

        #region 参数校验
        /// <summary>
        /// DES加解密配置参数有效性
        /// </summary>
        /// <param name="setting">DES加解密配置</param>
        /// <returns></returns>
        private static bool DESSettingValid(DESSetting setting)
        {
            if (setting == null)
                throw new ArgumentNullException("DESSetting不能为null(DESSetting cannot be null)");
            if (string.IsNullOrWhiteSpace(setting.SecretKey))
                throw new ArgumentException("DESSetting的私钥不能为null或空白字符(The private key of DESSetting cannot be null or whitespace characters)");
            if (setting.ModeType == SecureCipherModeType.CBC && string.IsNullOrWhiteSpace(setting.IV))
                throw new ArgumentException("在DES加解密CBC模式下IV不能为null或者空白字符(IV cannot be null or blank characters in DES encryption and decryption CBC mode)");
           
            return true;
        }

        /// <summary>
        /// AES加解密配置参数有效性
        /// </summary>
        /// <param name="setting">AES加解密配置</param>
        /// <returns></returns>
        private static bool AESSettingValid(AESSetting setting)
        {
            if (setting == null)
                throw new ArgumentNullException("AESSetting不能为null(AESSetting cannot be null)");
            if (string.IsNullOrWhiteSpace(setting.SecretKey))
                throw new ArgumentException("AESSetting的私钥不能为null或空白字符(The private key of AESSetting cannot be null or whitespace characters)");
            if (setting.ModeType == SecureCipherModeType.CBC && string.IsNullOrWhiteSpace(setting.IV))
                throw new ArgumentException("在AES加解密CBC模式下IV不能为null或者空白字符(IV cannot be null or blank characters in AES encryption and decryption CBC mode)");
            if (!KeySizeRange.HasAnyValue(u => u == setting.KeySize))
                throw new ArgumentException("AESSetting中的KeySize有效值为:128、160、192、224或256(The valid KeySize values in AESSetting are: 128, 160, 192, 224, or 256)");

            return true;
        }

        /// <summary>
        /// RSA加解密配置参数有效性
        /// </summary>
        /// <param name="setting">RSA加解密配置</param>
        /// <param name="secretKeyType">密钥类型</param>
        /// <returns></returns>
        private static bool RSASettingValid(RSASetting setting, AsymmetricSecretKeyType? secretKeyType = null)
        {
            if (setting == null)
                throw new ArgumentNullException("RSASetting不能为null(RSASetting cannot be null)");
            if (secretKeyType != AsymmetricSecretKeyType.PublicKey && string.IsNullOrWhiteSpace(setting.PrivateKey))
                throw new ArgumentException("RSASetting的私钥不能为null或空白字符(The private key of RSASetting cannot be null or whitespace characters)");
            if (secretKeyType != AsymmetricSecretKeyType.PrivateKey && string.IsNullOrWhiteSpace(setting.PublicKey))
                throw new ArgumentException("RSASetting的公钥不能为null或空白字符(The public key of RSASetting cannot be null or whitespace characters)");

            return true;
        }

        /// <summary>
        /// 字符串参数的有效性
        /// </summary>
        /// <param name="value">值</param>
        /// <param name="zh_cn">中文描述</param>
        /// <param name="en">英文解释</param>
        /// <returns></returns>
        private static bool StringParamValid(string value, string zh_cn = "值", string en = "value")
        {
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentException($"{zh_cn}不能为null或空白字符({en} cannot be null or whitespace characters)");

            return true;
        }
        #endregion

        #region 哈希算法
        /// <summary>
        /// 使用哈希算法加密
        /// </summary>
        /// <param name="plain">原文</param>
        /// <param name="hashAlgorithmType">哈希算法类型(default:MD5)</param>
        /// <returns></returns>
        public static string EncryptByHashAlgorithm(string plain, HashAlgorithmType hashAlgorithmType = HashAlgorithmType.MD5)
        {
            if (plain.IsNullOrWhiteSpace())
                throw new ArgumentException("明文不能为null或者空白字符");

            using var hashAlgorithm = HashAlgorithm.Create(hashAlgorithmType.GetDescriptionValue().ToLower());
            var buffer = Encoding.UTF8.GetBytes(plain);
            buffer = hashAlgorithm.ComputeHash(buffer);
            hashAlgorithm.Clear();

            return BitConverter.ToString(buffer).Replace("-", "").ToLower();
        }

        /// <summary>
        /// 使用哈希算法比较明文与密文的相等性
        /// </summary>
        /// <param name="plain">明文</param>
        /// <param name="ciphertext">密文</param>
        /// <param name="hashAlgorithmType">哈希算法类型(default:MD5)</param>
        /// <returns></returns>
        public static bool EqualsByHashAlgorithm(string plain, string ciphertext, HashAlgorithmType hashAlgorithmType = HashAlgorithmType.MD5)
        {
            return ciphertext.Equals(EncryptByHashAlgorithm(plain, hashAlgorithmType), StringComparison.Ordinal);
        }
        #endregion

        #region DES
        /// <summary>
        /// 生成DES密钥
        /// </summary>
        /// <param name="cipherModeType">加解密模式类型(default:CBC模式)</param>
        /// <param name="keySize">key的大小(default:256) 有效值为:128、160、192、224或256</param>
        /// <returns>(密钥,IV向量)</returns>
        public static (string, string?) GenerateDESKey(SecureCipherModeType cipherModeType = SecureCipherModeType.CBC)
        {
            return GenerateSymmetricAlgorithmKey(SymmetricAlgorithmType.DES, cipherModeType);
        }

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="plain">明文</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string DESEncrypt(string plain, DESSetting setting)
        {
            _ = StringParamValid(plain, "明文", "plain");

            using var des = CreateDESCryptoServiceProvider(setting);
            var buffer = Encoding.UTF8.GetBytes(plain);
            using var ms = new MemoryStream();
            using var cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(buffer, 0, buffer.Length);
            cs.FlushFinalBlock();
            buffer = ms.ToArray();

            return BitConverter.ToString(buffer).Replace("-", "").ToLower();
        }

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="plain">明文</param>
        /// <returns></returns>
        public string DESEncrypt(string plain)
        {
            return DESEncrypt(plain, DESSetting);
        }

        /// <summary> 
        /// DES解密 
        /// </summary> 
        /// <param name="ciphertext">密文</param> 
        /// <param name="setting">秘钥</param>
        /// <returns></returns>
        public static string DESDecrypt(string ciphertext, DESSetting setting)
        {
            _ = StringParamValid(ciphertext, "密文", "ciphertext");

            using var des = CreateDESCryptoServiceProvider(setting);
            //转换hex格式数据为byte数组
            var buffer = new byte[ciphertext.Length / 2];
            for (var i = 0; i < buffer.Length; i++)
            {
                buffer[i] = (byte)Convert.ToInt32(ciphertext.Substring(i * 2, 2), 16);
            }
            using var ms = new MemoryStream();
            using var cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(buffer, 0, buffer.Length);
            cs.FlushFinalBlock();

            return Encoding.UTF8.GetString(ms.ToArray());
        }

        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="ciphertext">密文</param> 
        /// <returns></returns>
        public string DESDecrypt(string ciphertext)
        {
            return DESDecrypt(ciphertext, DESSetting);
        }
        #endregion

        #region AES
        /// <summary>
        /// 生成AES密钥
        /// </summary>
        /// <param name="cipherModeType">加解密模式类型(default:CBC模式)</param>
        /// <param name="keySize">key的大小(default:256) 有效值为:128、160、192、224或256</param>
        /// <returns>(密钥,IV向量)</returns>
        public static (string, string?) GenerateAESKey(SecureCipherModeType cipherModeType = SecureCipherModeType.CBC, int keySize = 256)
        {
            return GenerateSymmetricAlgorithmKey(SymmetricAlgorithmType.AES, cipherModeType, keySize);
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="plain">明文</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string AESEncrypt(string plain, AESSetting setting)
        {
            _ = StringParamValid(plain, "明文", "plain");

            using var rijndaelManaged = CreateRijndaelManaged(setting);
            using var iCryptoTransform = rijndaelManaged.CreateEncryptor();
            var buffer = Encoding.UTF8.GetBytes(plain);
            buffer = iCryptoTransform.TransformFinalBlock(buffer, 0, buffer.Length);
            return BitConverter.ToString(buffer).Replace("-", "").ToLower();
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="plain">明文</param>
        /// <returns></returns>
        public string AESEncrypt(string plain)
        {
            return AESEncrypt(plain, AESSetting);
        }

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="ciphertext">密文</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string AESDecrypt(string ciphertext, AESSetting setting)
        {
            _ = StringParamValid(ciphertext, "密文", "ciphertext");

            using var rijndaelManaged = CreateRijndaelManaged(setting);
            using var iCryptoTransform = rijndaelManaged.CreateDecryptor();
            //转换hex格式数据为byte数组
            var buffer = new byte[ciphertext.Length / 2];
            for (var i = 0; i < buffer.Length; i++)
            {
                buffer[i] = (byte)Convert.ToInt32(ciphertext.Substring(i * 2, 2), 16);
            }
            buffer = iCryptoTransform.TransformFinalBlock(buffer, 0, buffer.Length);
            return Encoding.UTF8.GetString(buffer);
        }

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="ciphertext">密文</param>
        /// <returns></returns>
        public string AESDecrypt(string ciphertext)
        {
            return AESDecrypt(ciphertext, AESSetting);
        }
        #endregion

        #region RSA

        #region 密钥生成及存储
        /// <summary>
        /// 生成RSA非对称密钥对
        /// </summary>
        /// <param name="genFormatType">生成格式类型(default:PKCS8)</param>
        /// <param name="keySize">key的大小(default:1024)</param>
        /// <returns>(公钥,私钥)</returns>
        public static (string, string) GenerateRSAKey(RSAGenFormatType genFormatType = RSAGenFormatType.PKCS8, int keySize = 1024)
        {
            using var rsa = new RSACryptoServiceProvider();
            rsa.KeySize = keySize;
            if (genFormatType == RSAGenFormatType.PKCS8)
            {
                //使用pkcs8填充方式导出
                return (rsa.ExportSubjectPublicKeyInfo().ToBase64String(), rsa.ExportPkcs8PrivateKey().ToBase64String());
            }
            else
            {
                //使用pkcs1填充方式导出
                return (rsa.ExportRSAPublicKey().ToBase64String(), rsa.ExportRSAPrivateKey().ToBase64String());
            }
        }

        /// <summary>
        /// 将密钥保存到Pem文件
        /// </summary>
        /// <param name="secretKey">密钥</param>
        /// <param name="secretKeyType">密钥类型</param>
        /// <param name="pemFilePath">pem文件路径</param>
        /// <returns></returns>
        public static bool SecretKeyWriteToPem(string secretKey, AsymmetricSecretKeyType secretKeyType, string pemFilePath)
        {
            _ = StringParamValid(secretKey);

            var pemObject = new PemObject(secretKeyType == AsymmetricSecretKeyType.PrivateKey ? "RSA PRIVATE KEY" : "RSA PUBLIC KEY", secretKey.ToBytesFromBase64String());
            if (File.Exists(pemFilePath))
            {
                File.Delete(pemFilePath);
            }
            using var fileStream = new FileStream(pemFilePath, FileMode.OpenOrCreate, FileAccess.Write);
            using var sw = new StreamWriter(fileStream);
            var writer = new PemWriter(sw);
            writer.WriteObject(pemObject);
            sw.Flush();

            return true;
        }

        /// <summary>
        /// 从Pem文件中读取密钥
        /// </summary>
        /// <param name="pemFilePath">pem文件路径</param>
        /// <returns></returns>
        public static string ReadSecretKeyFromPem(string pemFilePath)
        {
            using var fileStream = new FileStream(pemFilePath, FileMode.Open, FileAccess.Read);
            using var sw = new StreamReader(fileStream);
            var writer = new PemReader(sw);
            return writer.ReadPemObject().Content.ToBase64String();
        }

        /// <summary>
        /// 将密钥保存到xml中
        /// </summary>
        /// <param name="secretKey">密钥</param>
        /// <param name="secretKeyType">密钥类型</param>
        /// <param name="genFormatType">生成格式类型</param>
        /// <returns></returns>
        public static string SecretKeyWriteToXml(string secretKey, AsymmetricSecretKeyType secretKeyType, RSAGenFormatType genFormatType)
        {
            _ = StringParamValid(secretKey);

            var isPrivateKey = secretKeyType == AsymmetricSecretKeyType.PrivateKey;
            var setting = new RSASetting() { GenFormatType = genFormatType };
            if (isPrivateKey)
                setting.PrivateKey = secretKey;
            else
                setting.PublicKey = secretKey;

            using var rsa = CreateRSACryptoServiceProvider(setting, secretKeyType);
            return rsa.ToXmlString(isPrivateKey);
        }

        /// <summary>
        /// 从xml中读取密钥
        /// </summary>
        /// <param name="xml">xml</param>
        /// <param name="secretKeyType">密钥类型</param>
        /// <param name="genFormatType">生成格式类型</param>
        /// <returns></returns>
        public static string ReadSecretKeyFromXml(string xml, AsymmetricSecretKeyType secretKeyType, RSAGenFormatType genFormatType)
        {
            _ = StringParamValid(xml);

            var isPrivateKey = secretKeyType == AsymmetricSecretKeyType.PrivateKey;
            var usePkcs8 = genFormatType == RSAGenFormatType.PKCS8;

            using var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(xml);
            if (isPrivateKey)
            {
                return usePkcs8 ? rsa.ExportPkcs8PrivateKey().ToBase64String() : rsa.ExportRSAPrivateKey().ToBase64String();
            }
            return usePkcs8 ? rsa.ExportSubjectPublicKeyInfo().ToBase64String() : rsa.ExportRSAPublicKey().ToBase64String();
        }

        /// <summary>
        /// 从Pfx文件获取RSA非对称密钥对
        /// </summary>
        /// <param name="pfxFilePath">pfx文件路径</param>
        /// <param name="password">密码</param>
        /// <returns>(公钥,私钥)</returns>
        public static (string, string) ReadSecretKeyFromPfx(string pfxFilePath, string password)
        {
            var x509Certificate2 = new X509Certificate2(pfxFilePath, password, X509KeyStorageFlags.Exportable);

            return (x509Certificate2.GetRSAPublicKey().ExportRSAPublicKey().ToBase64String(), x509Certificate2.GetRSAPrivateKey().ExportRSAPrivateKey().ToBase64String());
        }
        /// <summary>
        /// 从Crt文件中读取公钥
        /// </summary>
        /// <param name="crtFilePath">crt文件路径</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        public static string ReadPublicKeyFromCrt(string crtFilePath, string password)
        {
            var x509Certificate2 = new X509Certificate2(crtFilePath, password, X509KeyStorageFlags.Exportable);
            return x509Certificate2.GetRSAPublicKey().ExportRSAPublicKey().ToBase64String();
        }
        #endregion

        #region 格式转换
        /// <summary>
        /// Pkcs1转Pkcs8
        /// </summary>
        /// <param name="secretKey">密钥</param>
        /// <param name="secretKeyType">密钥类型</param>
        /// <returns></returns>
        public static string RSAPkcs1ToPkcs8(string secretKey, AsymmetricSecretKeyType secretKeyType)
        {
            var isPrivateKey = secretKeyType == AsymmetricSecretKeyType.PrivateKey;
            var buffer = secretKey.ToBytesFromBase64String();

            using var rsa = new RSACryptoServiceProvider();
            if (isPrivateKey)
            {
                rsa.ImportRSAPrivateKey(buffer, out _);
                return rsa.ExportPkcs8PrivateKey().ToBase64String();
            }
            else
            {
                rsa.ImportRSAPublicKey(buffer, out _);
                return rsa.ExportSubjectPublicKeyInfo().ToBase64String();
            }
        }

        /// <summary>
        /// Pkcs8转Pkcs1
        /// </summary>
        /// <param name="secretKey">密钥</param>
        /// <param name="secretKeyType">密钥类型</param>
        /// <returns></returns>
        public static string RSAPkcs8ToPkcs1(string secretKey, AsymmetricSecretKeyType secretKeyType)
        {
            var isPrivateKey = secretKeyType == AsymmetricSecretKeyType.PrivateKey;
            var buffer = secretKey.ToBytesFromBase64String();

            using var rsa = new RSACryptoServiceProvider();
            if (isPrivateKey)
            {
                rsa.ImportPkcs8PrivateKey(buffer, out _);
                return rsa.ExportRSAPrivateKey().ToBase64String();
            }
            else
            {
                rsa.ImportSubjectPublicKeyInfo(buffer, out _);
                return rsa.ExportRSAPublicKey().ToBase64String();
            }
        }
        #endregion

        #region 加解密、签名验证
        /// <summary>
        /// RSA公钥加密
        /// </summary>
        /// <param name="plain">明文</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string RSAEncrypt(string plain, RSASetting setting)
        {
            _ = StringParamValid(plain, "明文", "plain");

            using var rsa = CreateRSACryptoServiceProvider(setting, AsymmetricSecretKeyType.PublicKey);
            var buffer = Encoding.UTF8.GetBytes(plain);
            buffer = rsa.Encrypt(buffer, false);

            return BitConverter.ToString(buffer).Replace("-", "").ToLower();
        }

        /// <summary>
        /// RSA公钥加密
        /// </summary>
        /// <param name="plain">明文</param>
        /// <returns></returns>
        public string RSAEncrypt(string plain)
        {
            return RSAEncrypt(plain, RSASetting);
        }

        /// <summary>
        /// RSA私钥解密
        /// </summary>
        /// <param name="ciphertext">密文</param>
        /// <param name="setting">加解密配置</param>
        /// <returns></returns>
        public static string RSADecrypt(string ciphertext, RSASetting setting)
        {
            _ = StringParamValid(ciphertext, "密文", "ciphertext");

            using var rsa = CreateRSACryptoServiceProvider(setting, AsymmetricSecretKeyType.PrivateKey);
            //转换hex格式数据为byte数组
            var buffer = new byte[ciphertext.Length / 2];
            for (var i = 0; i < buffer.Length; i++)
            {
                buffer[i] = (byte)Convert.ToInt32(ciphertext.Substring(i * 2, 2), 16);
            }
            buffer = rsa.Decrypt(buffer, false);
            return Encoding.UTF8.GetString(buffer);
        }

        /// <summary>
        /// RSA私钥解密
        /// </summary>
        /// <param name="ciphertext">密文</param>
        /// <returns></returns>
        public string RSADecrypt(string ciphertext)
        {
            return RSADecrypt(ciphertext, RSASetting);
        }

        /// <summary>
        /// RSA私钥生成签名
        /// </summary>
        /// <param name="originalValue">原始值</param>
        /// <param name="setting">加解密配置</param>
        /// <param name="hashAlgorithmType">哈希算法类型</param>
        /// <returns></returns>
        public static string RSAGenerateSign(string originalValue, RSASetting setting, HashAlgorithmType hashAlgorithmType)
        {
            _ = StringParamValid(originalValue, "原始值", "originalValue");

            using var rsa = CreateRSACryptoServiceProvider(setting, AsymmetricSecretKeyType.PrivateKey);
            var buffer = Encoding.UTF8.GetBytes(originalValue);
            buffer = rsa.SignData(buffer, HashAlgorithm.Create(hashAlgorithmType.GetDescriptionValue()));

            return BitConverter.ToString(buffer).Replace("-", "").ToLower();
        }

        /// <summary>
        /// RSA私钥生成签名
        /// </summary>
        /// <param name="originalValue">原始值</param>
        /// <param name="hashAlgorithmType">哈希算法类型</param>
        /// <returns></returns>
        public string RSAGenerateSign(string originalValue, HashAlgorithmType hashAlgorithmType)
        {
            return RSAGenerateSign(originalValue, RSASetting, hashAlgorithmType);
        }

        /// <summary>
        /// RSA公钥验证签名
        /// </summary>
        /// <param name="originalValue">原始值</param>
        /// <param name="signature">签名</param>
        /// <param name="setting">加解密配置</param>
        /// <param name="hashAlgorithmType">哈希算法类型</param>
        /// <returns></returns>
        public static bool RSAVerify(string originalValue, string signature, RSASetting setting, HashAlgorithmType hashAlgorithmType)
        {
            _ = StringParamValid(originalValue, "原始值", "originalValue");
            _ = StringParamValid(signature, "签名", "signature");

            using var rsa = CreateRSACryptoServiceProvider(setting, AsymmetricSecretKeyType.PublicKey);
            //转换hex格式数据为byte数组
            var buffer = new byte[signature.Length / 2];
            for (var i = 0; i < buffer.Length; i++)
            {
                buffer[i] = (byte)Convert.ToInt32(signature.Substring(i * 2, 2), 16);
            }
            return rsa.VerifyData(Encoding.UTF8.GetBytes(originalValue), HashAlgorithm.Create(hashAlgorithmType.GetDescriptionValue()), buffer);
        }

        /// <summary>
        /// RSA公钥验证签名
        /// </summary>
        /// <param name="originalValue">原始值</param>
        /// <param name="signature">签名</param>
        /// <param name="hashAlgorithmType">哈希算法类型</param>
        /// <returns></returns>
        public bool RSAVerify(string originalValue, string signature, HashAlgorithmType hashAlgorithmType)
        {
            return RSAVerify(originalValue, signature, RSASetting, hashAlgorithmType);
        }
        #endregion

        #endregion
    }
}
