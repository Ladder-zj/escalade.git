using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using EasyTool;

namespace Escalade.Common
{
    /// <summary>
    /// 安全性、加解密相关Util
    /// </summary>
    [Obsolete("该实用程序暂不推荐使用，当然如果你仍要选择使用它也没有任何问题.(This utility is currently not recommended for use, but if you still want to choose to use it, there is no problem.)", false)]
    public sealed class SecurityUtil : IDisposable
    {
        private IDisposable? _settingListener;
        private byte[]? _aesKey;
        private byte[]? _aesIv;
        private RSA? _rsaPublic = default!;
        private RSA? _rsaPrivate;

        /// <summary>
        /// ctor
        /// </summary>
        public SecurityUtil()
        {

        }

        /// <summary>
        /// ctor
        /// </summary>
        public SecurityUtil(AESSetting aESSetting = null, RSASetting rSASetting = null)
        {
            if (aESSetting != null)
                InitAES(aESSetting);

            if (rSASetting != null)
                InitRSA(rSASetting);
        }

        /// <summary>
        /// 初始化AES加解密
        /// </summary>
        /// <param name="aESSetting">AES加解密配置</param>
        private void InitAES(AESSetting aESSetting)
        {
            AESParamValid(aESSetting.SecretKey, aESSetting.IV);
            _aesKey = aESSetting.SecretKey.HexStringToByte();
            _aesIv = aESSetting.IV.HexStringToByte();
        }

        /// <summary>
        /// 初始化RSA加解密
        /// </summary>
        /// <param name="rSASetting">RSA加解密配置</param>
        private void InitRSA(RSASetting rSASetting)
        {
            CreateRSA();
            _rsaPublic.ImportRSAPublicKey(Convert.FromBase64String(rSASetting.PublicKey), out _);
            _rsaPrivate.ImportRSAPrivateKey(Convert.FromBase64String(rSASetting.PrivateKey), out _);
        }

        /// <summary>
        /// 创建RSA实例
        /// </summary>
        private void CreateRSA()
        {
            _rsaPublic ??= RSA.Create();
            _rsaPrivate ??= RSA.Create();
        }

        /// <summary>
        /// AES相关参数的有效性
        /// </summary>
        /// <param name="aesKey">密钥</param>
        /// <param name="aesIv">初始化向量</param>
        /// <param name="length">长度 default：32</param>
        private static void AESParamValid(string aesKey, string aesIv, int length = 32)
        {
            var pattern = "^[0-9A-Fa-f]{" + length + "}$";
            if (!RegexUtil.IsMatch(aesKey, pattern))
                throw new ArgumentException($"AESKey需要是长度为{length}位的十六进制字符", nameof(aesKey));

            if (!RegexUtil.IsMatch(aesIv, pattern))
                throw new ArgumentException($"AESIv需要是长度为{length}位的十六进制字符", nameof(aesIv));
        }

        /// <summary>
        /// MD5使用StringBuilder方式
        /// </summary>
        /// <param name="plain">原文</param>
        /// <returns></returns>
        public static string MD5StringBuilder(string plain)
        {
            using var md5 = MD5.Create();
            var inputBytes = Encoding.UTF8.GetBytes(plain);
            var hashBytes = md5.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            foreach (var hashByte in hashBytes)
            {
                sb.Append(hashByte.ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// MD5使用BitConverter方式
        /// </summary>
        /// <param name="plain">原文</param>
        /// <returns></returns>
        public static string MD5BitConverter(string plain)
        {
            using var md5 = MD5.Create();
            var inputBytes = Encoding.UTF8.GetBytes(plain);
            var hashBytes = md5.ComputeHash(inputBytes);
            return BitConverter.ToString(hashBytes).Replace("-", "");
        }

        /// <summary>
        /// MD5使用StringConcat方式
        /// </summary>
        /// <param name="plain">原文</param>
        /// <returns></returns>
        public static string MD5StringConcat(string plain)
        {
            using var md5 = MD5.Create();
            var inputBytes = Encoding.UTF8.GetBytes(plain);
            var hashBytes = md5.ComputeHash(inputBytes);
            var output = string.Empty;
            foreach (var hashByte in hashBytes)
            {
                output += hashByte.ToString("X2");
            }
            return output;
        }

        /// <summary>
        /// 获取流的MD5哈希值
        /// </summary>
        /// <param name="stream">stream</param>
        /// <returns></returns>
        public static string MD5Stream(Stream stream)
        {
            using MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(stream);
            byte[] b = md5.Hash;
            md5.Clear();
            StringBuilder sb = new StringBuilder(32);
            for (int i = 0; i < b.Length; i++)
            {
                sb.Append(b[i].ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="plain">原文</param>
        /// <returns></returns>
        public byte[] AESEncrypt(byte[] plain)
        {
            return AESEncrypt(plain, _aesKey, _aesIv);
        }

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <returns></returns>
        public byte[] AESDecrypt(byte[] encrypted)
        {
            return AESDecrypt(encrypted, _aesKey, _aesIv);
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="plain">原文</param>
        /// <returns></returns>
        public string AESEncrypt(string plain)
        {
            return Convert.ToBase64String(AESEncrypt(Encoding.UTF8.GetBytes(plain)));
        }

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <returns></returns>
        public string AESDecrypt(string encrypted)
        {
            return Encoding.UTF8.GetString(AESDecrypt(Convert.FromBase64String(encrypted)));
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="plain">原文</param>
        /// <param name="aesKey">AES密钥</param>
        /// <param name="aesIv">AES初始化向量</param>
        /// <returns></returns>
        public static byte[] AESEncrypt(byte[] plain, byte[] aesKey, byte[] aesIv)
        {
            if (aesKey == null || aesIv == null)
                throw new InvalidOperationException("please provider key and iv setting");

            using var aes = Aes.Create();
            aes.Key = aesKey;
            aes.IV = aesIv;
            using var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
            using var stream = new MemoryStream();
            using var crypto = new CryptoStream(stream, encryptor, CryptoStreamMode.Write);
            crypto.Write(plain, 0, plain.Length);
            crypto.FlushFinalBlock();
            return stream.ToArray();
        }

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="aesKey">AES密钥</param>
        /// <param name="aesIv">AES初始化向量</param>
        /// <returns></returns>
        public static byte[] AESDecrypt(byte[] encrypted, byte[] aesKey, byte[] aesIv)
        {
            if (aesKey == null || aesIv == null)
                throw new InvalidOperationException("please provider key and iv setting");

            using var aes = Aes.Create();
            aes.Key = aesKey;
            aes.IV = aesIv;
            using var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
            using var stream = new MemoryStream();
            using var crypto = new CryptoStream(stream, decryptor, CryptoStreamMode.Write);
            crypto.Write(encrypted, 0, encrypted.Length);
            crypto.FlushFinalBlock();
            return stream.ToArray();
        }

        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="plain">原文</param>
        /// <param name="aesKey">AES密钥</param>
        /// <param name="aesIv">AES初始化向量</param>
        /// <returns></returns>
        public static string AESEncrypt(string plain, string aesKey, string aesIv)
        {
            AESParamValid(aesKey, aesIv);
            return Convert.ToBase64String(AESEncrypt(Encoding.UTF8.GetBytes(plain), aesKey.HexStringToByte(), aesIv.HexStringToByte()));
        }

        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="aesKey">AES密钥</param>
        /// <param name="aesIv">AES初始化向量</param>
        /// <returns></returns>
        public static string AESDecrypt(string encrypted, string aesKey, string aesIv)
        {
            AESParamValid(aesKey, aesIv);
            return Encoding.UTF8.GetString(AESDecrypt(Convert.FromBase64String(encrypted), aesKey.HexStringToByte(), aesIv.HexStringToByte()));
        }

        /// <summary>
        /// 生成指定长度的十六进制字符
        /// </summary>
        /// <param name="length">指定长度</param>
        /// <param name="textCaseType">文本大小写类型</param>
        /// <returns></returns>
        public static string GenerateHexString(int length = 32, TextCaseType textCaseType = TextCaseType.Default)
        {
            var random = new Random();
            var sb = new StringBuilder(length);

            while (sb.Length < length)
            {
                var buffer = new byte[length];
                random.NextBytes(buffer);
                var hex = BitConverter.ToString(buffer).Replace("-", "");
                _ = sb.Append(hex);
            }

            var res = sb.ToString()[..length];
            if (textCaseType == TextCaseType.Upper)
                return res.ToUpper();
            else if (textCaseType == TextCaseType.Lower)
                return res.ToLower();
            else
                return res;
        }

        /// <summary>
        /// 生成RSA公私钥
        /// </summary>
        /// <returns>(公钥，私钥)</returns>
        public static (string, string) GenerateRSAKey()
        {
            using (var rsa = RSA.Create())
            {
                var publicKey = Convert.ToBase64String(rsa.ExportRSAPublicKey());
                var privateKey = Convert.ToBase64String(rsa.ExportRSAPrivateKey());
                return (publicKey, privateKey);
            }
        }

        /// <summary>
        /// RSA加密
        /// </summary>
        /// <param name="plain">原文</param>
        /// <returns></returns>
        public byte[] RSAEncrypt(byte[] plain)
        {
            if (_rsaPublic == null)
            {
                throw new InvalidOperationException("please provider RSA public key");
            }
            return _rsaPublic.Encrypt(plain, RSAEncryptionPadding.OaepSHA256);
        }

        /// <summary>
        /// RSA解密
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <returns></returns>
        public byte[] RSADecrypt(byte[] encrypted)
        {
            if (_rsaPrivate == null)
            {
                throw new InvalidOperationException("please provider RSA private key");
            }
            return _rsaPrivate.Decrypt(encrypted, RSAEncryptionPadding.OaepSHA256);
        }

        /// <summary>
        /// RSA加密
        /// </summary>
        /// <param name="plain">原文</param>
        /// <returns></returns>
        public string RSAEncrypt(string plain)
        {
            return Convert.ToBase64String(RSAEncrypt(Encoding.UTF8.GetBytes(plain)));
        }

        /// <summary>
        /// RSA解密
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <returns></returns>
        public string RSADecrypt(string encrypted)
        {
            return Encoding.UTF8.GetString(RSADecrypt(Convert.FromBase64String(encrypted)));
        }

        /// <summary>
        /// RSA加密
        /// </summary>
        /// <param name="plain">原文</param>
        /// <param name="publicKey">公钥</param>
        /// <returns></returns>
        public static byte[] RSAEncrypt(byte[] plain, string publicKey)
        {
            byte[] encryptedData;

            using (RSA rsa = RSA.Create())
            {
                rsa.ImportRSAPublicKey(Convert.FromBase64String(publicKey), out _);
                encryptedData = rsa.Encrypt(plain, RSAEncryptionPadding.OaepSHA256);
            }

            return encryptedData;
        }

        /// <summary>
        /// RSA解密
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="privateKey">私钥</param>
        /// <returns></returns>
        public static byte[] RSADecrypt(byte[] encrypted, string privateKey)
        {
            byte[] decryptedData;

            using (RSA rsa = RSA.Create())
            {
                rsa.ImportRSAPrivateKey(Convert.FromBase64String(privateKey), out _);
                decryptedData = rsa.Decrypt(encrypted, RSAEncryptionPadding.OaepSHA256);
            }

            return decryptedData;
        }

        /// <summary>
        /// RSA加密
        /// </summary>
        /// <param name="plain">原文</param>
        /// <param name="publicKey">公钥</param>
        /// <returns></returns>
        public static string RSAEncrypt(string plain, string publicKey)
        {
            return Convert.ToBase64String(RSAEncrypt(Encoding.UTF8.GetBytes(plain), publicKey));
        }

        /// <summary>
        /// RSA解密
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="privateKey">私钥</param>
        /// <returns></returns>
        public static string RSADecrypt(string encrypted, string privateKey)
        {
            return Encoding.UTF8.GetString(RSADecrypt(Convert.FromBase64String(encrypted), privateKey));
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (_settingListener != null)
            {
                _settingListener.Dispose();
                _settingListener = null;
            }
            if (_rsaPublic != null)
            {
                _rsaPublic.Dispose();
                _rsaPublic = null;
            }
            if (_rsaPrivate != null)
            {
                _rsaPrivate.Dispose();
                _rsaPrivate = null;
            }
        }
    }
}
