using System;
using System.Collections.Generic;

namespace Escalade.Common
{
    /// <summary>
    /// 树结构实用程序
    /// </summary>
    /// <typeparam name="T">T</typeparam>
    public class TreeUtil<T> where T : ITreeComponent
    {
        /// <summary>
        /// 根父级Id
        /// </summary>
        public long RootParantId { get; set; }

        /// <summary>
        /// 树结构构建
        /// </summary>
        /// <param name="sourceDatas">数据源</param>
        /// <returns></returns>
        public IList<T> TreeBuilder(IList<T> sourceDatas)
        {
            return TreeBuilder(sourceDatas, null);
        }

        /// <summary>
        /// 树结构构建
        /// </summary>
        /// <param name="sourceDatas">数据源</param>
        /// <param name="action">其它行为</param>
        /// <returns></returns>
        public IList<T> TreeBuilder(IList<T> sourceDatas, Action<T> action)
        {
            foreach (var sourceData in sourceDatas)
            {
                if (sourceData.GetParentId() == RootParantId)
                {
                    TreeChildrenBuilder(sourceDatas, sourceData, new List<T>(), action);
                    return new List<T>() { sourceData };
                }
            }

            return null!;
        }

        /// <summary>
        /// 树结构子项构建
        /// </summary>
        /// <param name="sourceDatas">数据源</param>
        /// <param name="data">当前项数据</param>
        /// <param name="children">子项数据</param>
        /// <param name="action">其它行为</param>
        private void TreeChildrenBuilder(IList<T> sourceDatas, T data, List<T> children, Action<T> action = null)
        {
            var list = new List<T>();
            if (action.IsNotNull())
                action(data);

            foreach (var sourceData in sourceDatas)
            {
                if (sourceData.GetParentId() == data.GetId())
                    list.Add(sourceData);
            }
            list.ForEach(u => TreeChildrenBuilder(sourceDatas, u, new List<T>(), action));
            children.AddRange(list);
            data.SetChildren(children);
        }
    }
}
