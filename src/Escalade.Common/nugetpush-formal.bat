@echo off
set api_key=oy3a5vi4hotfpfocmhuq5rhhrtp4wk3ufhpmjkzvrjql3u
set source_api_uri=https://api.nuget.org/v3/index.json
set package_version=1.1.3

:: 执行打包pack
echo pack Escalade.Common
dotnet pack --output .nuget\v%package_version% -p:PackageVersion=%package_version%

:: 切换到该目录下
cd .nuget\v%package_version%

:: 执行推送
echo=
dotnet nuget push -k %api_key% -s %source_api_uri% Escalade.Common.%package_version%.nupkg

echo=
pause