﻿namespace Escalade.ImageDeal.Common.Const;

/// <summary>
/// 作者信息常量
/// </summary>
public static class AuthorInfoConst
{
    /// <summary>
    /// 作者名
    /// </summary>
    public const string AuthorName = "Escalade-Ladder";

    /// <summary>
    /// 邮箱
    /// </summary>
    public const string Email = "1544192464@qq.com";
}
