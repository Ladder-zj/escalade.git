﻿namespace Escalade.ImageDeal.Common.Const;

/// <summary>
/// 版本常量
/// </summary>
public static class VersionConst
{
    /// <summary>
    /// 图片处理功能版本
    /// </summary>
    public const string EscaladeImageDealVersion = "v1.0.0";
}
