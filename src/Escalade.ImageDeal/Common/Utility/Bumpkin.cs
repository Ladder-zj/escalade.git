﻿namespace Escalade.ImageDeal.Common.Utility;

/// <summary>
/// Bumpkin common help static method
/// </summary>
public static class Bumpkin
{
    /// <summary>
    /// Welcome
    /// </summary>
    /// <param name="message">功能启动消息</param>
    /// <param name="version">版本号</param>
    public static void Welcome(string message, string authorName = "匿名", string version = "暂无版本")
    {
        Console.WriteLine("---------------------------");
        Console.WriteLine(message);
        Console.WriteLine($"作者:{authorName}");
        Console.WriteLine($"版本号:{version}");
        Console.WriteLine("---------------------------");
    }

    /// <summary>
    /// 执行退出
    /// </summary>
    /// <param name="consoleKey">按键key</param>
    /// <returns></returns>
    public static bool ExecuteExit(ConsoleKey consoleKey)
    {
        Console.WriteLine();
        return Console.ReadKey().Key == consoleKey;
    }
}
