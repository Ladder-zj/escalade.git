﻿namespace Escalade.ImageDeal.Model.Enum;

/// <summary>
/// 图片处理类型
/// </summary>
public enum ImageDealType
{
    /// <summary>
    /// 裁剪
    /// </summary>
    [Description("裁剪")]
    Cut = 1,

    /// <summary>
    /// 缩略
    /// </summary>
    [Description("缩略")]
    Thumbnail = 2,

    /// <summary>
    /// 文字水印
    /// </summary>
    [Description("文字水印")]
    TextWatermark = 3,

    /// <summary>
    /// 图片水印
    /// </summary>
    [Description("图片水印")]
    ImageWatermark = 4
}
