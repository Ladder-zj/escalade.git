﻿namespace Escalade.ImageDeal.Model.Policy;

/// <summary>
/// 裁剪图片处理参数
/// </summary>
public class CutImageInput
{
    /// <summary>
    /// 起始裁剪点X坐标
    /// </summary>
    public int CutStartPointX { get; set; }

    /// <summary>
    /// 起始裁剪点Y坐标
    /// </summary>
    public int CutStartPointY { get; set; }

    /// <summary>
    /// 裁剪宽度
    /// </summary>
    public int CutWidth { get; set; }

    /// <summary>
    /// 裁剪高度
    /// </summary>
    public int CutHeight { get; set; }
}
