﻿namespace Escalade.ImageDeal.Model.Policy;

/// <summary>
/// 图片处理参数
/// </summary>
public class ImageDealInput
{
    /// <summary>
    /// 源图片路径
    /// </summary>
    public string SourceImagePath { get; set; } = string.Empty;

    /// <summary>
    /// 新生成的图片路径
    /// </summary>
    public string TargetImagePath { get; set; } = string.Empty;

    /// <summary>
    /// 裁剪图片Input
    /// </summary>
    public CutImageInput? CutImageInput { get; set; }

    /// <summary>
    /// 缩略图片Input
    /// </summary>
    public ThumbnailImageInput? ThumbnailImageInput { get; set; }

    /// <summary>
    /// 图片添加文字水印Input
    /// </summary>
    public TextWatermarkImgaeInput? TextWatermarkImgaeInput { get; set; }

    /// <summary>
    /// 图片添加图片水印Input
    /// </summary>
    public ImageWatermarkImageInput? ImageWatermarkImageInput { get; set; }
}
