﻿namespace Escalade.ImageDeal.Model.Policy;

/// <summary>
/// 图片添加图片水印处理参数
/// </summary>
public class ImageWatermarkImageInput
{
    /// <summary>
    /// 水印图像文件路径
    /// </summary>
    public string WatermarkImagePath { get; set; } = string.Empty;

    /// <summary>
    /// 水印图像的起始X坐标
    /// </summary>
    public int StartPointX { get; set; }

    /// <summary>
    /// 水印图像的起始Y坐标
    /// </summary>
    public int StartPointY { get; set; }
}
