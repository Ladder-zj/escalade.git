﻿namespace Escalade.ImageDeal.Model.Policy;

/// <summary>
/// 图片文字水印处理入参
/// </summary>
public class TextWatermarkImgaeInput
{
    /// <summary>
    /// 水印文字
    /// </summary>
    public string WaterText { get; set; } = string.Empty;

    /// <summary>
    /// 水印文字字体
    /// </summary>
    public Font WaterTextFont { get; set; } = new Font("微软雅黑", 40, FontStyle.Bold);

    /// <summary>
    /// 水印文字笔触
    /// </summary>
    public Brush WaterTextBrush { get; set; } = Brushes.Azure;

    /// <summary>
    /// 水印图像的起始X坐标
    /// </summary>
    public int StartPointX { get; set; }

    /// <summary>
    /// 水印图像的起始Y坐标
    /// </summary>
    public int StartPointY { get; set; }
}
