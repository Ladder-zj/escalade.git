﻿namespace Escalade.ImageDeal.Model.Policy;

public class ThumbnailImageInput
{
    /// <summary>
    /// 缩略框的宽度
    /// </summary>
    public int FrameWidth { get; set; }

    /// <summary>
    /// 缩略框的高度
    /// </summary>
    public int FrameHeight { get; set; }
}
