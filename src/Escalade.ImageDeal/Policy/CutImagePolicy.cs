﻿namespace Escalade.ImageDeal.Policy;

/// <summary>
/// 裁剪图片策略
/// </summary>
public class CutImagePolicy : ImageDealPolicy
{
    /// <summary>
    /// 图片处理
    /// </summary>
    public override void ImageDeal()
    {
        while (true)
        {
            try
            {
                imageDealInput.CutImageInput = new CutImageInput();
                Console.WriteLine("\n当前您正在操作【裁剪图片】功能");
                Console.WriteLine("请输入源图片路径:");
                imageDealInput.SourceImagePath = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(imageDealInput.SourceImagePath) || !File.Exists(imageDealInput.SourceImagePath))
                {
                    Console.WriteLine("您输入的源图片不存在，按任意键继续，按F1退出当前功能操作");
                    if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                        break;
                    else
                        continue;
                }
                Console.WriteLine("请输入起始裁剪点X坐标:");
                imageDealInput.CutImageInput.CutStartPointX = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入起始裁剪点Y坐标:");
                imageDealInput.CutImageInput.CutStartPointY = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入裁剪宽度:");
                imageDealInput.CutImageInput.CutWidth = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入裁剪高度:");
                imageDealInput.CutImageInput.CutHeight = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入新生成的目标图片文件路径:");
                imageDealInput.TargetImagePath = Console.ReadLine();
                //调用裁剪方法
                iu.PictureCut(imageDealInput.SourceImagePath, imageDealInput.CutImageInput.CutStartPointX, imageDealInput.CutImageInput.CutStartPointY, imageDealInput.CutImageInput.CutWidth, imageDealInput.CutImageInput.CutHeight, imageDealInput.TargetImagePath);

                Console.WriteLine("执行成功!");
                Console.WriteLine($"新生成的目标图片文件路径:{imageDealInput.TargetImagePath}");
                Console.WriteLine("按任意键继续，按F1退出当前功能操作");
                if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                    break;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"抱歉系统遇到未知错误,错误反馈邮箱地址:{AuthorInfoConst.Email}\n错误信息:{ex.Message}\n按任意键继续，按F1退出当前功能操作");
                if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                    break;
                else
                    continue;
            }
        }
    }
}
