﻿namespace Escalade.ImageDeal.Policy;

/// <summary>
/// 图片处理上下文
/// </summary>
public class ImageDealContext
{
    /// <summary>
    /// 图片处理策略
    /// </summary>
    private readonly ImageDealPolicy _policy;

    /// <summary>
    /// ctor
    /// </summary>
    public ImageDealContext(ImageDealPolicy policy)
    {
        _policy = policy;
    }

    /// <summary>
    /// 图片处理
    /// </summary>
    /// <param name="input">参数/param>
    public void ImageDeal()
    {
        _policy.ImageDeal();
    }
}
