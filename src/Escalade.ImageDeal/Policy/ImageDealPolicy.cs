﻿namespace Escalade.ImageDeal.Policy;

/// <summary>
/// 图片处理策略
/// </summary>
public abstract class ImageDealPolicy
{
    /// <summary>
    /// 图片处理参数
    /// </summary>
    protected ImageDealInput imageDealInput { get; set; } = new ImageDealInput();

    /// <summary>
    /// 图片处理Utility
    /// </summary>
    protected IUtility iu = new Utility();

    /// <summary>
    /// 图片处理方法
    /// </summary>
    public abstract void ImageDeal();
}
