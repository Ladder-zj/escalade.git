﻿namespace Escalade.ImageDeal.Policy;

/// <summary>
/// 图片添加图片水印策略
/// </summary>
public class ImageWatermarkImagePolicy : ImageDealPolicy
{
    /// <summary>
    /// 图片处理
    /// </summary>
    public override void ImageDeal()
    {
        while (true)
        {
            try
            {
                imageDealInput.ImageWatermarkImageInput = new ImageWatermarkImageInput();
                Console.WriteLine("\n当前您正在操作【添加图片水印】功能");
                Console.WriteLine("请输入源图片路径:");
                imageDealInput.SourceImagePath = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(imageDealInput.SourceImagePath) || !File.Exists(imageDealInput.SourceImagePath))
                {
                    Console.WriteLine("您输入的源图片不存在，按任意键继续，按F1退出当前功能操作");
                    if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                        break;
                    else
                        continue;
                }
                Console.WriteLine("请输入水印图像文件路径:");
                imageDealInput.ImageWatermarkImageInput.WatermarkImagePath = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(imageDealInput.ImageWatermarkImageInput.WatermarkImagePath) || !File.Exists(imageDealInput.ImageWatermarkImageInput.WatermarkImagePath))
                {
                    Console.WriteLine("您输入的水印图像文件不存在，按任意键继续，按F1退出当前功能操作");
                    if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                        break;
                    else
                        continue;
                }
                Console.WriteLine("请输入水印图像的起始X坐标:");
                imageDealInput.ImageWatermarkImageInput.StartPointX = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入水印图像的起始Y坐标:");
                imageDealInput.ImageWatermarkImageInput.StartPointY = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入新生成的目标图片文件路径:");
                imageDealInput.TargetImagePath = Console.ReadLine();
                //调用添加文字水印方法
                iu.PictureImageWatermarkAsync(imageDealInput.SourceImagePath, imageDealInput.ImageWatermarkImageInput.WatermarkImagePath, imageDealInput.ImageWatermarkImageInput.StartPointX, imageDealInput.ImageWatermarkImageInput.StartPointY, imageDealInput.TargetImagePath);

                Console.WriteLine("执行成功!");
                Console.WriteLine($"新生成的目标图片文件路径:{imageDealInput.TargetImagePath}");
                Console.WriteLine("按任意键继续，按F1退出当前功能操作");
                if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                    break;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"抱歉系统遇到未知错误,错误反馈邮箱地址:{AuthorInfoConst.Email}\n错误信息:{ex.Message}\n按任意键继续，按F1退出当前功能操作");
                if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                    break;
                else
                    continue;
            }
        }
    }
}
