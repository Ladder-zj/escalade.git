﻿namespace Escalade.ImageDeal.Policy;

/// <summary>
/// 图片添加文字水印策略
/// </summary>
public class TextWatermarkImagePolicy : ImageDealPolicy
{
    /// <summary>
    /// 图片处理
    /// </summary>
    public override void ImageDeal()
    {
        while (true)
        {
            try
            {
                imageDealInput.TextWatermarkImgaeInput = new TextWatermarkImgaeInput();
                Console.WriteLine("\n当前您正在操作【添加文字水印】功能");
                Console.WriteLine("请输入源图片路径:");
                imageDealInput.SourceImagePath = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(imageDealInput.SourceImagePath) || !File.Exists(imageDealInput.SourceImagePath))
                {
                    Console.WriteLine("您输入的源图片不存在，按任意键继续，按F1退出当前功能操作");
                    if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                        break;
                    else
                        continue;
                }
                Console.WriteLine("请输入水印文字:");
                imageDealInput.TextWatermarkImgaeInput.WaterText = Console.ReadLine();
                Console.WriteLine("请输入水印文字字体\n输入1：【华文新魏】\n输入2：【宋体】\n默认：【微软雅黑】");
                var font = Console.ReadKey();
                if (font.Key == ConsoleKey.NumPad1)
                {
                    imageDealInput.TextWatermarkImgaeInput.WaterTextFont = new Font("华文新魏", 40, FontStyle.Bold);
                }
                else if (font.Key == ConsoleKey.NumPad2)
                {
                    imageDealInput.TextWatermarkImgaeInput.WaterTextFont = new Font("宋体", 40, FontStyle.Bold);
                }
                else
                {
                    imageDealInput.TextWatermarkImgaeInput.WaterTextFont = new Font("微软雅黑", 40, FontStyle.Bold);
                }
                Console.WriteLine("\n请输入水印图像的起始X坐标:");
                imageDealInput.TextWatermarkImgaeInput.StartPointX = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入水印图像的起始Y坐标:");
                imageDealInput.TextWatermarkImgaeInput.StartPointY = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入新生成的目标图片文件路径:");
                imageDealInput.TargetImagePath = Console.ReadLine();
                //调用添加文字水印方法
                iu.PictureTextWatermark(imageDealInput.SourceImagePath, imageDealInput.TextWatermarkImgaeInput.WaterText, imageDealInput.TextWatermarkImgaeInput.WaterTextFont, imageDealInput.TextWatermarkImgaeInput.WaterTextBrush, imageDealInput.TextWatermarkImgaeInput.StartPointX, imageDealInput.TextWatermarkImgaeInput.StartPointY, imageDealInput.TargetImagePath);

                Console.WriteLine("执行成功!");
                Console.WriteLine($"新生成的目标图片文件路径:{imageDealInput.TargetImagePath}");
                Console.WriteLine("按任意键继续，按F1退出当前功能操作");
                if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                    break;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"抱歉系统遇到未知错误,错误反馈邮箱地址:{AuthorInfoConst.Email}\n错误信息:{ex.Message}\n按任意键继续，按F1退出当前功能操作");
                if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                    break;
                else
                    continue;
            }
        }
    }
}
