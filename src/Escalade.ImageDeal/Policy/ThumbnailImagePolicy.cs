﻿namespace Escalade.ImageDeal.Policy;

/// <summary>
/// 缩略图片策略
/// </summary>
public class ThumbnailImagePolicy : ImageDealPolicy
{
    /// <summary>
    /// 图片处理
    /// </summary>
    public override void ImageDeal()
    {
        while (true)
        {
            try
            {
                imageDealInput.ThumbnailImageInput = new ThumbnailImageInput();
                Console.WriteLine("\n当前您正在操作【缩略图片】功能");
                Console.WriteLine("请输入源图片路径:");
                imageDealInput.SourceImagePath = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(imageDealInput.SourceImagePath) || !File.Exists(imageDealInput.SourceImagePath))
                {
                    Console.WriteLine("您输入的源图片不存在，按任意键继续，按F1退出当前功能操作");
                    if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                        break;
                    else
                        continue;
                }
                Console.WriteLine("请输入缩略框的宽度:");
                imageDealInput.ThumbnailImageInput.FrameWidth = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入缩略框的高度:");
                imageDealInput.ThumbnailImageInput.FrameHeight = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("请输入新生成的目标图片文件路径:");
                imageDealInput.TargetImagePath = Console.ReadLine();
                //调用缩略图片方法
                iu.PictureThumbnail(imageDealInput.SourceImagePath, imageDealInput.ThumbnailImageInput.FrameWidth, imageDealInput.ThumbnailImageInput.FrameHeight, imageDealInput.TargetImagePath);

                Console.WriteLine("执行成功!");
                Console.WriteLine($"新生成的目标图片文件路径:{imageDealInput.TargetImagePath}");
                Console.WriteLine("按任意键继续，按F1退出当前功能操作");
                if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                    break;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"抱歉系统遇到未知错误,错误反馈邮箱地址:{AuthorInfoConst.Email}\n错误信息:{ex.Message}\n按任意键继续，按F1退出当前功能操作");
                if (Bumpkin.ExecuteExit(ConsoleKey.F1))
                    break;
                else
                    continue;
            }
        }
    }
}
