﻿namespace Escalade.ImageDeal.Services;

/// <summary>
/// 服务基类
/// </summary>
public abstract class BaseService
{
    /// <summary>
    /// 启动
    /// </summary>
    public abstract void Start();
}
