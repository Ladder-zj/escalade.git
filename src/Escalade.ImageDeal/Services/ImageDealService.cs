﻿namespace Escalade.ImageDeal.Services;

/// <summary>
/// 图片处理服务
/// </summary>
public class ImageDealService : BaseService
{
    /// <summary>
    /// 图片处理上下文
    /// </summary>
    private static ImageDealContext? _context;

    /// <summary>
    /// 继续说明
    /// </summary>
    private const string continueMessage = "按任意键继续操作";

    /// <summary>
    /// 退出说明
    /// </summary>
    private const string exitMessage = "按下F1退出程序";

    /// <summary>
    /// 联合操作说明
    /// </summary>
    private string continueUnionExitMessage = continueMessage + exitMessage;

    /// <summary>
    /// 启动
    /// </summary>
    public override void Start()
    {
        Bumpkin.Welcome("欢迎使用Escalade图片处理工具", AuthorInfoConst.AuthorName, VersionConst.EscaladeImageDealVersion);
        while (true)
        {
            Console.WriteLine("\n请选择要进行的图片操作\n输入1：【裁剪图片】\n输入2：【缩略图片】\n输入3：【添加文字水印】\n输入4：【添加图片水印】");
            //操作
            int.TryParse(Console.ReadLine(), out var operate);
            switch ((ImageDealType)operate)
            {
                case ImageDealType.Cut:
                    _context = new ImageDealContext(new CutImagePolicy());
                    break;
                case ImageDealType.Thumbnail:
                    _context = new ImageDealContext(new ThumbnailImagePolicy());
                    break;
                case ImageDealType.TextWatermark:
                    _context = new ImageDealContext(new TextWatermarkImagePolicy());
                    break;
                case ImageDealType.ImageWatermark:
                    _context = new ImageDealContext(new ImageWatermarkImagePolicy());
                    break;
                default:
                    _context = null;
                    Console.WriteLine($"抱歉，没识别到您想操作的功能,{continueUnionExitMessage}");
                    break;
            }
            //执行策略
            _context?.ImageDeal();

            if (_context == null && Bumpkin.ExecuteExit(ConsoleKey.F1))
                break;
        }
    }
}
