global using BenchmarkDotNet.Running;
global using BenchmarkDotNet.Attributes;

global using Escalade.Common;
global using Escalade.Test.Benchmark.Tests;