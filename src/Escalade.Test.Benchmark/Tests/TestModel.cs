namespace Escalade.Test.Benchmark.Tests;

//[MaxColumn, MinColumn]
[MemoryDiagnoser]
public class TestModel
{
    private readonly int _length = 100;
    private readonly string _appendValue = "appendValue";

    [Params("Source1")]
    public string SourceValue { get; set; }

    [Benchmark]
    public string AppendTest()
    {
        for (var i = 0; i < _length; i++)
            SourceValue = SourceValue.AppendIF(true, _appendValue);

        return SourceValue;
    }
}
