namespace Escalade.Test.MSTest;

/// <summary>
/// 配置类
/// </summary>
public class AppSettings
{
    /// <summary>
    /// 测试字段
    /// </summary>
    public string Test { get; set; }
}
