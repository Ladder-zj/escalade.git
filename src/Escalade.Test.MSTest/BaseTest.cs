namespace Escalade.Test.MSTest;

/// <summary>
/// Test抽象基类
/// </summary>
public abstract class BaseTest
{
    protected IConfiguration Configuration { get; }

    protected IServiceProvider ServiceProvider { get; }

    protected ILifetimeScope Container { get; }

    protected AppSettings AppSettings { get; }

    public BaseTest()
    {
        var localConfigurationBuilder = new ConfigurationBuilder();
        _ = localConfigurationBuilder.AddJsonFile("Config/appsettings.json", true)
                                     .AddJsonFile("Config/appsettings.Development.json", true);
        ConfigureConfiguration(localConfigurationBuilder);

        Configuration = localConfigurationBuilder.Build();
        AppSettings = Configuration.GetSection(nameof(AppSettings)).Get<AppSettings>();

        //init service colletion and register services
        var serviceCollection = new ServiceCollection();
        _ = serviceCollection.AddSingleton(Configuration);
        _ = serviceCollection.Configure<AppSettings>(Configuration.GetSection(nameof(AppSettings)));

        //configure service collection for sub class
        ConfigureService(serviceCollection);

        //init service provider factory with `ConfigureContainer`
        var serviceProviderFactory = new AutofacServiceProviderFactory(ConfigureContainer);
        var containerBuilder = serviceProviderFactory.CreateBuilder(serviceCollection);

        //build service provider
        ServiceProvider = serviceProviderFactory.CreateServiceProvider(containerBuilder);
        Container = ((AutofacServiceProvider)ServiceProvider).LifetimeScope;
    }

    /// <summary>
    /// configure configuration builder in sub class
    /// </summary>
    /// <param name="configurationBuilder"></param>
    public virtual void ConfigureConfiguration(IConfigurationBuilder configurationBuilder)
    {

    }

    /// <summary>
    /// configure service in sub class
    /// </summary>
    /// <param name="services"></param>
    public virtual void ConfigureService(IServiceCollection services)
    {

    }

    /// <summary>
    /// configure container in sub class
    /// </summary>
    /// <param name="builder"></param>
    public virtual void ConfigureContainer(ContainerBuilder builder)
    {

    }
}
