namespace Escalade.Test.MSTest;

/// <summary>
/// 测试模型抽象
/// </summary>
public abstract class BaseTestModel
{
    /// <summary>
    /// 初始化测试数据
    /// </summary>
    public abstract void Init();

    /// <summary>
    /// 重写ToString()
    /// </summary>
    /// <returns></returns>
    public abstract override string ToString();
}
