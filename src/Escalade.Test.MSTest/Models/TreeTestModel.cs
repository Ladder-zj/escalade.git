using System.Collections;

namespace Escalade.Test.MSTest;

/// <summary>
/// 树结构测试Model
/// </summary>
/// <param name="Id">Id</param>
/// <param name="ParentId">父级Id</param>
public record TreeTestModel(long Id, long ParentId) : ITreeComponent
{
    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 子项数据
    /// </summary>
    public List<TreeTestModel> Children { get; set; } = new List<TreeTestModel>();

    public long GetId() => Id;
    public long GetParentId() => ParentId;
    public void SetChildren(IList children) => Children = (List<TreeTestModel>)children;
}
