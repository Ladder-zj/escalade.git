namespace Escalade.Test.MSTest;

/// <summary>
/// 用户测试类
/// </summary>
public class User : BaseTestModel
{
    /// <summary>
    /// ctor
    /// </summary>
    public User()
    {
        Init();
    }

    /// <summary>
    /// Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 用户名
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// 年龄
    /// </summary>
    public int Age { get; set; }

    /// <summary>
    /// 初始化测试数据
    /// </summary>
    public override void Init()
    {
        Id = 1;
        UserName = "Escalade";
        Age = 23;
    }

    /// <summary>
    /// 重写ToString()
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return $"Id:{Id},UserName:{UserName},Age:{Age}";
    }
}
