namespace Escalade.Test.MSTest;

/// <summary>
/// Escalade.Common package test
/// </summary>
[TestClass]
public partial class CommonPackageTest : BaseTest
{
    public override void ConfigureService(IServiceCollection services)
    {
        //_ = services.AddIdWorker(0, () => { return 1; });
    }

    [TestMethod]
    public void TestMethod1()
    {
        var test = AppSettings.Test;
        var testBool = true;
        var res1 = testBool.ConvertToEnum(BooleanType.True, BooleanType.False);
        var res2 = testBool.ConvertToString(textCaseType: TextCaseType.Upper);

        Console.WriteLine(res1);
        Console.WriteLine(res2);

        var str1 = "1234567890一二三四五六七八九十零";
        var l1 = str1.Limit(5);
        var l2 = str1.Limit(5, 20);
        var l3 = str1.LimitAppendAfter(11, "...");
        var l4 = str1.LimitAppendAfter(11, -222, "是hihihi");
        var l5 = str1.AppendIF(true, AppendValueWayType.Before, "1", "2");
        var l6 = str1.AppendIF(true, "1");
        var l7 = str1.AppendIF(false, AppendValueWayType.After, "1", "3");
        var strList = new List<string>() { "3", "4", "5" };
        var l8 = str1.AppendIF(true, strList);

        Assert.IsTrue(true);
    }

    [DataTestMethod]
    [DataRow("test11")]
    public void MD5TestMethod(string plain)
    {
        var encrypt = SecurityUtil.MD5StringBuilder(plain);
        var encrypt1 = SecurityUtil.MD5StringBuilder(plain);

        Assert.AreEqual(encrypt, encrypt1);
    }

    [DataTestMethod]
    [DataRow("test11", "00112233445566778899AABBCCDDEEFF", "0123456789ABCDEF0123456789ABCDEF")]
    //[DataRow("test22", "3A5F9B2C8E147D6F0B9A8C7E", "4A5F9B2C8E147D6F0B9A8C7E")]
    public void AESTestMethod(string plain, string key, string iv)
    {
        var encrypt = SecurityUtil.AESEncrypt(plain, key, iv);
        var decrypt = SecurityUtil.AESDecrypt(encrypt, key, iv);

        Assert.AreEqual(plain, decrypt);
    }

    [DataTestMethod]
    [DataRow("1234567890")]
    public void RSATestMethod(string plain)
    {
        var (publicKey, privateKey) = SecurityUtil.GenerateRSAKey();
        using var rsa = new SecurityUtil(rSASetting: new RSASetting() { PrivateKey = privateKey, PublicKey = publicKey });
        var encrypt = rsa.RSAEncrypt(plain);
        var decrypt = rsa.RSADecrypt(encrypt);
        var encrypt1 = SecurityUtil.RSAEncrypt(plain, publicKey);
        var decrypt1 = SecurityUtil.RSADecrypt(encrypt, privateKey);

        var encrypt2 = plain.RSAEncrypt(publicKey);
        var decrypt2 = encrypt2.RSADecrypt(privateKey);

        Assert.AreEqual(plain, decrypt2);
        Assert.AreEqual(plain, decrypt1);
        Assert.AreEqual(plain, decrypt);
    }

    [TestMethod]
    public void NotNullTestMethod()
    {
        List<int> list = null;
        string str = null;
        str = str.NotNull();
        list = list.NotNull();

        Assert.IsTrue(str.IsNotNull() && list.IsNotNull());
    }

    [TestMethod]
    public void EnumerableTestMethod()
    {
        IEnumerable<string> s = null;
        s = s.NotNull();
        string str = null;
        str = str.NotNull("123");


        Assert.IsTrue(s.IsNotNull() && str.IsNotNull());
    }

    [TestMethod]
    public void SerializeTestMethod()
    {
        var user = new User();
        var userSer = user.SerializeToString();
        var userDes = userSer.DeserializeObject<User>();

        var userBytes = user.SerializeToBytes();
        var userDes2 = userBytes.DeserializeObject<User>();

        Console.WriteLine(user.ToString());
        Console.WriteLine(userDes.ToString());
        Console.WriteLine(userDes2.ToString());

        Assert.AreEqual(user.ToString(), userDes.ToString());
        Assert.AreEqual(user.ToString(), userDes2.ToString());
    }

    [TestMethod]
    public void OtherTestMethod()
    {
        var list = new List<string>() { "1", "2", "3" };
        var str = list.ToString();

        Assert.IsTrue(true);
    }

    [TestMethod]
    public void IdWorkerTestMethod()
    {
        var list = new List<long>();
        //IdWorkerUtil.CustomNextIdFunc = () => { return 2; };
        for (var i = 0; i < 1000; i++)
        {
            list.Add(IdWorkerUtil.NextId());
        }

        Assert.IsTrue(list.Distinct().Count() == list.Count);
    }
}
