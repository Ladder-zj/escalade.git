namespace Escalade.Test.MSTest;

/// <summary>
/// Escalade.Common package test
/// </summary>
public partial class CommonPackageTest : BaseTest
{
    [TestMethod]
    public void IPUtilTestMethod()
    {
        var localIPAddress = IPUtil.GetLocalIPAddress();
        var publicIPAddress = IPUtil.GetPublicIPAddress();
        var localWalnIPAddress = IPUtil.GetLocalWalnIPAddress();

        var localWalnIP = IPUtil.GetLocalWalnIP();
        var localIP = IPUtil.GetLocalIP();
        var localIPv6 = IPUtil.GetLocalIP(AddressFamily.InterNetworkV6);
        var localWalnIPv6 = IPUtil.GetLocalWalnIP(AddressFamily.InterNetworkV6);
        var publicIP = IPUtil.GetPublicIP();

        var b1 = IPUtil.IsIPv4(localIP);
        var b2 = IPUtil.IsIPv6(localIP);
        var b3 = IPUtil.IsIPv4(localIPv6);
        var b4 = IPUtil.IsIPv6(localIPv6);
        var b5 = IPUtil.IsIPv4(localWalnIPv6);
        var b6 = IPUtil.IsIPv6(localWalnIPv6);

        var eqb1 = IPUtil.IPEquals(localIP, "192.168.3.21");
        var eqb2 = IPUtil.IPEquals(localWalnIPAddress, IPAddress.Parse(localWalnIP));
        var eqb3 = IPUtil.IPEquals(publicIPAddress, IPAddress.Parse(publicIP));
        var eqb4 = IPUtil.IPEquals(localIPAddress, IPAddress.Parse(localIP));

        Assert.IsTrue(!eqb1 && eqb2 && eqb3 && eqb4);
    }
}
