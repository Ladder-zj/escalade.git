namespace Escalade.Test.MSTest;

/// <summary>
/// 多数据库执行脚本测试
/// </summary>
[TestClass]
public class MoreDatabaseExecScriptTest : BaseTest, IDisposable
{
    /// <summary>
    /// 主Db
    /// </summary>
    private ISqlSugarClient _db;

    /// <summary>
    /// 连接配置集合
    /// </summary>
    private List<ConnectionConfig> _connectionConfigs;

    /// <summary>
    /// 执行的脚本
    /// </summary>
    private readonly string _execScript = @"
create table [user](
Id bigint primary key,
Name nvarchar(50) not null
)
GO
insert into [user](Id,Name) values(1,'ladder')
GO
--insert into [user](Id,Name) values('2','escalade')
GO
create table tableA(
Name nvarchar(50) not null
)
";

    /// <summary>
    /// Dispose
    /// </summary>
    public void Dispose()
    {
        if (_db != null)
        {
            _db.Dispose();
            _db = null;
        }
        GC.SuppressFinalize(this);
    }

    [TestMethod]
    public void ExecScript()
    {
        _connectionConfigs = new List<ConnectionConfig>() {
            new ConnectionConfig(){ ConfigId="Test_01",DbType=SqlSugar.DbType.SqlServer,ConnectionString="server=.;uid=sa;pwd=12345;database=Test_01;",IsAutoCloseConnection=true },
            new ConnectionConfig(){ ConfigId="Test_02",DbType=SqlSugar.DbType.SqlServer,ConnectionString="server=.;uid=sa;pwd=12345;database=Test_02;",IsAutoCloseConnection=true },
            new ConnectionConfig(){ ConfigId="Test_03",DbType=SqlSugar.DbType.SqlServer,ConnectionString="server=.;uid=sa;pwd=12345;database=Test_03;",IsAutoCloseConnection=true }
        };
        //主Db
        _db = new SqlSugarClient(_connectionConfigs);

        //获取子Db
        //var test_1DB = _db.AsTenant().GetConnectionScope("Test_01");
        //var test_2DB = _db.AsTenant().GetConnectionScope("Test_02");
        //var test_3DB = _db.AsTenant().GetConnectionScope("Test_03");

        try
        {
            _db.AsTenant().BeginTran();
            foreach (var item in _connectionConfigs)
            {
                var childDb = _db.AsTenant().GetConnectionScope((string)item.ConfigId);
                _ = childDb.Ado.ExecuteCommandWithGo(_execScript);
            }
            _db.AsTenant().CommitTran();
        }
        catch
        {
            _db.AsTenant().RollbackTran();
            Assert.IsTrue(false);
        }

        Assert.IsTrue(true);
    }
}
