namespace Escalade.Test.MSTest;

/// <summary>
/// SecureUtilTest
/// </summary>
[TestClass]
public class SecureUtilTest : BaseTest
{
    private readonly DESSetting _dESSetting = new() { SecretKey = "9999", IV = "8888" };
    private readonly AESSetting _aESSetting = new() { SecretKey = "1111", IV = "2222", KeySize = 256 };
    private readonly RSASetting _rSASetting = new()
    {
        PublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDrP6sZ/8BvyZYRkFc4gnTCLkeecC9u4yrpmG3YagPW0UEO0uzz2g/MHSaVkNjlFMmoK7d8lVyOCcpxV5G5+989Khq6YUZp2NRBHc+NNRXU0fyG+wde/ogOgnAETaRLMN/W+brVuqXNXPayDZwGs43xxDIyrO1osY/RVmwfMfZJCQIDAQAB",
        PrivateKey = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAOs/qxn/wG/JlhGQVziCdMIuR55wL27jKumYbdhqA9bRQQ7S7PPaD8wdJpWQ2OUUyagrt3yVXI4JynFXkbn73z0qGrphRmnY1EEdz401FdTR/Ib7B17+iA6CcARNpEsw39b5utW6pc1c9rINnAazjfHEMjKs7Wixj9FWbB8x9kkJAgMBAAECgYB+tEIRtMAO9YJQX2+6zfjZBVgpq77AeLYKLsErCJ6cuwPRWiWpiU9s3l6DvAo2CCY8357qCQBauEdDGKkyhMhM/tKIIJUdoCgdvWNTEp2pTKEChxv7Jtu1aqdGBZC/0hjABHKd55cSQnyiE2fz3kRvjDQ9F4HqO+Z8HHF8hXl3qQJBAPCGXm0iLxwUwZMXF1KMs1BARRHSNnlwP16vmM2d8r/Zet1faGr2aEa2zQ0KIQi6JV6IxrSEHW2rrvt/91G6WZcCQQD6YmWXaoT+eO6JzIvykq0DstXVM5NJH2/S/zUdT+tuBKW61b7mmnNc9JDMzRVFdrTK8BEKN1rcB5WiflD9poZfAkEAqoS4YOqBN4lHiv4t3Ko/Xb+SuAWt9P4tDvWygONfe7+1KUhAgMRcPPXP/VSXAtblM1YIeWXIbz3t+gNI1Tol1QJBAJrYU1+q3ojJnSvXNg1ULG+HH7r0ZHfqWyuv7Nct37lwYlYDbkYK6qFoFkyx4B/PY8vFX6Rjy2sbvax6VfgoDh0CQQDdvb+kOavwo0Jr2uJrPM/G6dKKZoBpR4nol+y7ARLxa8x8qaQmULTQn4ZtZGIBZeGA6lGm4oU1n8XQu3VPu6jS"
    };

    public override void ConfigureService(IServiceCollection services)
    {
        _ = services.AddSecure(options =>
        {
            options.DESSetting = _dESSetting;
            options.AESSetting = _aESSetting;
            options.RSASetting = _rSASetting;
        });
    }

    [DataTestMethod]
    [DataRow("123456")]
    public void EncryptByHashAlgorithmTest(string plain)
    {
        var md5Encrypted = SecureUtil.EncryptByHashAlgorithm(plain, HashAlgorithmType.MD5);
        var sha1Encrypted = SecureUtil.EncryptByHashAlgorithm(plain, HashAlgorithmType.SHA1);
        var sha256Encrypted = SecureUtil.EncryptByHashAlgorithm(plain, HashAlgorithmType.SHA256);
        var sha384Encrypted = SecureUtil.EncryptByHashAlgorithm(plain, HashAlgorithmType.SHA384);
        var sha512Encrypted = SecureUtil.EncryptByHashAlgorithm(plain, HashAlgorithmType.SHA512);

        Console.WriteLine($"MD5:{md5Encrypted}");
        Console.WriteLine($"SHA1:{sha1Encrypted}");
        Console.WriteLine($"SHA256:{sha256Encrypted}");
        Console.WriteLine($"SHA384:{sha384Encrypted}");
        Console.WriteLine($"SHA512:{sha512Encrypted}");

        Assert.IsTrue(SecureUtil.EqualsByHashAlgorithm(plain, md5Encrypted, HashAlgorithmType.MD5)
                   && SecureUtil.EqualsByHashAlgorithm(plain, sha1Encrypted, HashAlgorithmType.SHA1)
                   && SecureUtil.EqualsByHashAlgorithm(plain, sha256Encrypted, HashAlgorithmType.SHA256)
                   && SecureUtil.EqualsByHashAlgorithm(plain, sha384Encrypted, HashAlgorithmType.SHA384)
                   && SecureUtil.EqualsByHashAlgorithm(plain, sha512Encrypted, HashAlgorithmType.SHA512));
    }

    [TestMethod]
    public void DESSecretKeyGenTest()
    {
        var (key, iv) = SecureUtil.GenerateDESKey();
        //var (key, iv) = SecureUtil.GenerateDESKey(cipherModeType: SecureCipherModeType.ECB);
        Console.WriteLine("DES密钥:" + key);
        Console.WriteLine("DES IV:" + iv);

        Assert.IsNotNull(key);
    }

    [DataTestMethod]
    [DataRow("123456")]
    public void DESTest(string plain)
    {
        var secureUtil = ServiceProvider.GetRequiredService<SecureUtil>();

        //执行加密
        var ciphertext = secureUtil.DESEncrypt(plain);
        //执行解密
        var decrypt = secureUtil.DESDecrypt(ciphertext);

        Console.WriteLine("DES私钥:" + secureUtil.DESSetting.SecretKey);
        Console.WriteLine("DES初始向量:" + secureUtil.DESSetting.IV);

        Assert.AreEqual(plain, decrypt);
    }

    [DataTestMethod]
    [DataRow("1234567890")]
    public void DESStaticMethodTest(string plain)
    {
        //执行加密
        var ciphertext = SecureUtil.DESEncrypt(plain, _dESSetting);
        //执行解密
        var decrypt = SecureUtil.DESDecrypt(ciphertext, _dESSetting);

        Console.WriteLine("DES私钥:" + _dESSetting.SecretKey);
        Console.WriteLine("DES初始向量:" + _dESSetting.IV);

        Assert.AreEqual(plain, decrypt);
    }

    [TestMethod]
    public void AESSecretKeyGenTest()
    {
        var (key, iv) = SecureUtil.GenerateAESKey();
        //var (key, iv) = SecureUtil.GenerateAESKey(cipherModeType: SecureCipherModeType.ECB);
        Console.WriteLine("AES密钥:" + key);
        Console.WriteLine("AES IV:" + iv);

        Assert.IsNotNull(key);
    }

    [DataTestMethod]
    [DataRow("123456")]
    public void AESTest(string plain)
    {
        var secureUtil = ServiceProvider.GetRequiredService<SecureUtil>();

        //执行加密
        var ciphertext = secureUtil.AESEncrypt(plain);
        //执行解密
        var decrypt = secureUtil.AESDecrypt(ciphertext);

        Console.WriteLine("AES私钥:" + secureUtil.AESSetting.SecretKey);
        Console.WriteLine("AES初始向量:" + secureUtil.AESSetting.IV);

        Assert.AreEqual(plain, decrypt);
    }

    [DataTestMethod]
    [DataRow("1234567890")]
    public void AESStaticMethodTest(string plain)
    {
        //执行加密
        var ciphertext = SecureUtil.AESEncrypt(plain, _aESSetting);
        //执行解密
        var decrypt = SecureUtil.AESDecrypt(ciphertext, _aESSetting);

        Console.WriteLine("AES私钥:" + _aESSetting.SecretKey);
        Console.WriteLine("AES初始向量:" + _aESSetting.IV);

        Assert.AreEqual(plain, decrypt);
    }

    [TestMethod]
    public void RSAGenKeyTest()
    {
        var (key1, key2) = SecureUtil.GenerateRSAKey();
        Console.WriteLine(key1 + "\n" + key2);

        Assert.IsTrue(true);
    }

    [DataTestMethod]
    [DataRow("1234567890")]
    public void RSATest(string plain)
    {
        var secureUtil = ServiceProvider.GetRequiredService<SecureUtil>();

        //执行加密
        var ciphertext = secureUtil.RSAEncrypt(plain);
        //执行解密
        var decrypt = secureUtil.RSADecrypt(ciphertext);

        Console.WriteLine("RSA公钥:" + secureUtil.RSASetting.PublicKey);
        Console.WriteLine("RSA私钥:" + secureUtil.RSASetting.PrivateKey);

        Assert.AreEqual(plain, decrypt);
    }

    [DataTestMethod]
    [DataRow("1234567890")]
    public void RSAStaticMethodTest(string plain)
    {
        //执行加密
        var ciphertext = SecureUtil.RSAEncrypt(plain, _rSASetting);
        //执行解密
        var decrypt = SecureUtil.RSADecrypt(ciphertext, _rSASetting);

        Console.WriteLine("RSA公钥:" + _rSASetting.PublicKey);
        Console.WriteLine("RSA私钥:" + _rSASetting.PrivateKey);

        Assert.AreEqual(plain, decrypt);
    }

    [DataTestMethod]
    [DataRow("1234567890")]
    public void RSASignAndVerifyTest(string originalValue)
    {
        var secureUtil = ServiceProvider.GetRequiredService<SecureUtil>();

        //签名
        var sign_1 = secureUtil.RSAGenerateSign(originalValue, HashAlgorithmType.MD5);
        //验证
        var verify_1 = secureUtil.RSAVerify(originalValue, sign_1, HashAlgorithmType.MD5);
        Console.WriteLine($"sign_1:{sign_1}");

        //静态方法测试
        var sign_2 = SecureUtil.RSAGenerateSign(originalValue, _rSASetting, HashAlgorithmType.MD5);
        var verify_2 = SecureUtil.RSAVerify(originalValue, sign_1, _rSASetting, HashAlgorithmType.MD5);
        Console.WriteLine($"sign_2:{sign_2}");

        Assert.IsTrue(verify_1 && verify_2);
    }

    [DataTestMethod]
    [DataRow(@"Resources\ssl\test.pfx", "123456")]
    public void ReadSecretKeyFromPfxTest(string path, string pwd)
    {
        var finalPath = Path.Combine(Directory.GetCurrentDirectory(), path);
        var (key1, key2) = SecureUtil.ReadSecretKeyFromPfx(finalPath, pwd);

        Console.WriteLine("RSA公钥:" + key1);
        Console.WriteLine("RSA私钥:" + key2);

        Assert.IsTrue(!string.IsNullOrWhiteSpace(key1) && !string.IsNullOrWhiteSpace(key2));
    }

    [DataTestMethod]
    [DataRow(@"Resources\ssl\test.crt", "")]
    public void ReadPublicKeyFromCrtTest(string path, string pwd)
    {
        var finalPath = Path.Combine(Directory.GetCurrentDirectory(), path);
        var publicKey = SecureUtil.ReadPublicKeyFromCrt(finalPath, pwd);

        Console.WriteLine("RSA公钥:" + publicKey);

        Assert.IsTrue(!string.IsNullOrWhiteSpace(publicKey));
    }

    [TestMethod]
    public void RSAWriteAndReadSecretKeyFromXmlTest()
    {
        //生成密钥
        var (publicKey, privateKey) = SecureUtil.GenerateRSAKey(RSAGenFormatType.PKCS8);
        //保存密钥到xml
        var publicKeyXmlStr = SecureUtil.SecretKeyWriteToXml(publicKey, AsymmetricSecretKeyType.PublicKey, RSAGenFormatType.PKCS8);
        var privateKeyXmlStr = SecureUtil.SecretKeyWriteToXml(privateKey, AsymmetricSecretKeyType.PrivateKey, RSAGenFormatType.PKCS8);

        //从xml中读取
        var readKey1 = SecureUtil.ReadSecretKeyFromXml(publicKeyXmlStr, AsymmetricSecretKeyType.PublicKey, RSAGenFormatType.PKCS8);
        var readKey2 = SecureUtil.ReadSecretKeyFromXml(privateKeyXmlStr, AsymmetricSecretKeyType.PrivateKey, RSAGenFormatType.PKCS8);

        var res1 = readKey1.Equals(publicKey, StringComparison.Ordinal);
        var res2 = readKey2.Equals(privateKey, StringComparison.Ordinal);

        Console.WriteLine("RSA公钥:" + readKey1);
        Console.WriteLine("RSA私钥:" + readKey2);

        Assert.IsTrue(res1 && res2);
    }

    [TestMethod]
    public void RSAPkcs1OrPkcs8ConvertMethodTest()
    {
        //生成pkcs8密钥
        var (publicKey, privateKey) = SecureUtil.GenerateRSAKey(RSAGenFormatType.PKCS8);

        //pkcs8 -> pkcs1
        var pkcs1PublickKey = SecureUtil.RSAPkcs8ToPkcs1(publicKey, AsymmetricSecretKeyType.PublicKey);
        var pkcs1PrivateKey = SecureUtil.RSAPkcs8ToPkcs1(privateKey, AsymmetricSecretKeyType.PrivateKey);

        Console.WriteLine("RSA公钥(pkcs1):" + pkcs1PublickKey);
        Console.WriteLine("RSA私钥(pkcs1):" + pkcs1PrivateKey);

        //pkcs1 -> pkcs8
        var pkcs8PublickKey = SecureUtil.RSAPkcs1ToPkcs8(pkcs1PublickKey, AsymmetricSecretKeyType.PublicKey);
        var pkcs8PrivateKey = SecureUtil.RSAPkcs1ToPkcs8(pkcs1PrivateKey, AsymmetricSecretKeyType.PrivateKey);

        var res1 = pkcs8PublickKey.Equals(publicKey, StringComparison.Ordinal);
        var res2 = pkcs8PrivateKey.Equals(privateKey, StringComparison.Ordinal);

        Console.WriteLine("RSA公钥(pkcs8):" + pkcs8PublickKey);
        Console.WriteLine("RSA私钥(pkcs8):" + pkcs8PrivateKey);

        Assert.IsTrue(res1 && res2);
    }

    [DataTestMethod]
    [DataRow(@"Resources\ssl\test.pub", @"Resources\ssl\test.pem")]
    public void RSAWriteAndReadSecretKeyFromPemTest(string publickKeyPemFilePath, string privateKeyPemFilePath)
    {
        var finalPublickKeyPemFilePath = Path.Combine(Directory.GetCurrentDirectory(), publickKeyPemFilePath);
        var finalPrivateKeyPemFilePath = Path.Combine(Directory.GetCurrentDirectory(), privateKeyPemFilePath);

        //生成密钥
        var (publicKey, privateKey) = SecureUtil.GenerateRSAKey(RSAGenFormatType.PKCS8);
        //保存密钥到文件
        _ = SecureUtil.SecretKeyWriteToPem(publicKey, AsymmetricSecretKeyType.PublicKey, finalPublickKeyPemFilePath);
        _ = SecureUtil.SecretKeyWriteToPem(privateKey, AsymmetricSecretKeyType.PrivateKey, finalPrivateKeyPemFilePath);

        //从文件中读取
        var readKey1 = SecureUtil.ReadSecretKeyFromPem(finalPublickKeyPemFilePath);
        var readKey2 = SecureUtil.ReadSecretKeyFromPem(finalPrivateKeyPemFilePath);

        var res1 = readKey1.Equals(publicKey, StringComparison.Ordinal);
        var res2 = readKey2.Equals(privateKey, StringComparison.Ordinal);

        Console.WriteLine("RSA公钥:" + readKey1);
        Console.WriteLine("RSA私钥:" + readKey2);

        Assert.IsTrue(res1 && res2);
    }
}
