namespace Escalade.Test.MSTest;

/// <summary>
/// TreeUtilTest
/// </summary>
[TestClass]
public class TreeUtilTest
{
    [TestMethod]
    public void TreeBuilderTest()
    {
        var sourceDatas = new List<TreeTestModel>();

        for (var i = 1; i <= 1; i++)
        {
            sourceDatas.Add(new TreeTestModel(i, 0) { Name = "根节点" });
            for (var j = 2; j <= 3; j++)
            {
                sourceDatas.Add(new TreeTestModel(j, i) { Name = $"节点第一层{j}" });
                for (var k = 1; k <= 3; k++)
                {
                    var id = k * j * 100;
                    sourceDatas.Add(new TreeTestModel(id, j) { Name = $"节点第二层{id}" });
                }
            }
        }

        var treeUtil = new TreeUtil<TreeTestModel>();
        //执行树结构构建
        var treeRes = treeUtil.TreeBuilder(sourceDatas, u => { u.Name = u.Name.Append("--action追加的--"); });
        var jsonRes = JsonConvert.SerializeObject(treeRes);
        Console.WriteLine(jsonRes);

        Assert.IsTrue(treeRes.HasAnyValue() && !jsonRes.IsNullOrWhiteSpace());
    }
}
